var fs = require("fs"),util = require('util');
var uuid = require('uuid');
module.exports = function(Container) {

  Container.afterRemote('upload', function (context, _upload, _next) {
      var filename=_upload.result.files.file[0].name;
      var path='server/storage/container1/';
      var destpath='server/storage/authjsfiles/';
      var destifilename=uuid.v1();
      var fname=_upload.result.files.file[0].name.split(".");
      var method_name=fname[0].split("-");
      fs.readFile(path+filename, 'utf8', function (err,data) {
          if (err) {
            return console.log(err);
          }
            var append_txt='module.exports='+method_name[1]+';';
            fs.appendFile(path+filename, append_txt, function (err) {
               var source = fs.createReadStream(path+filename)
               var dest = fs.createWriteStream(destpath+destifilename+'.js');
               source.pipe(dest);
               source.on('end', function() { console.log("done")});
               source.on('error', function(err) { console.log("Error")});
               fs.unlink(path+filename,function(){
                     context.res.send({"name":destifilename});
               });
            });

      });

    });
};
