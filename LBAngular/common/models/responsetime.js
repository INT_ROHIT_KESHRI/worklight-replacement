module.exports = function(Responsetime) {
  Responsetime.observe('before save', function(ctx, next) {
    if(ctx.isNewInstance){
      ctx.instance.created=new Date();
      ctx.instance.modified=new Date();
      ctx.instance.status='fast';
      ctx.instance.flag=1
      next();
    } else{
      ctx.data.modified=new Date();
      next();
    }
  });
  
  Responsetime.observe('before delete',function(ctx,next){
    next();
  });
};
