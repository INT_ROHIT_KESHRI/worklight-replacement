module.exports = function(Restclient) {
  Restclient.getmethod = function(cb) {
    request({
              uri: 'API_URI',
              method: 'get'
            }, function(error, response, body) {
              response.json(body);
            });
        };

  Restclient.remoteMethod(
    'getmethod',
    {
      http: {path: '/getmethod', verb: 'get'},
      returns: {arg: 'status', type: 'string'}
    }
  );

};
