var request = require("request");

var loopback = require('loopback');
var app = module.exports = loopback();
var parseString = require('xml2js').parseString;

module.exports = function(Vikash) {

	
  /**** User Method to get User details ****/
  Vikash.usersxml=function(req,cb) {
		request({
            uri: "http://10.0.8.13/api/xml/users",
            method: "GET"
        }, function(error, response, body) {
			parseString(body,{trim: false,explicitArray :false,explicitRoot:false}, function (err, result) {
				cb(null, result); 
			});
			 
		});
  };
  
  Vikash.remoteMethod(
    'usersxml',
    {
      http: {path: '/usersxml', verb: 'get'},
	  accepts:{ arg: 'data', type: 'object', http: { source: 'req' } },
      returns: {arg: 'status', type: 'string'}
    }
  );
  
  Vikash.afterRemote('usersxml',function(context,remoteMethodOutput,next){	
			
		    context.res.json(remoteMethodOutput.status);
		   
  });
	
};