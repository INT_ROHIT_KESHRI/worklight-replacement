var NodeCache = require("node-cache");
var myCache = new NodeCache();
var uuid = require('node-uuid');
var fs = require('fs');

module.exports = function(Cache) {
  Cache.observe('before save', function(ctx, next) {
		if(ctx.isNewInstance){
			ctx.instance.created=new Date();
			ctx.instance.modified=new Date();
      ctx.instance.cachkey=uuid.v4();
			next();
		} else{
      ctx.data.modified=new Date();
    //  ctx.data.flag=1;
			next();
		}
	});
  /*Cache.observe('after save', function(ctx, next) {
		if(ctx.isNewInstance==false){
      fs.appendFile('../LBServer/log.txt', 'data to append', function (err) {
      });
			next();
		}else{
      next();
    }
	});*/
  Cache.observe('before delete',function(ctx,next){
		var id=ctx.where.id;
		Cache.find({where:{id:id}},function(err,cache){
      console.log("Cache"+cache[0].id);
			var key_name=cache[0].cachkey;
      mykeys = myCache.keys();
      console.log("Keys"+mykeys);
      myCache.del(key_name);
      console.log("Keys"+mykeys);
			next();
		});

	});
};
