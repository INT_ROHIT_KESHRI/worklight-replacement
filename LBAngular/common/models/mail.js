var http = require("http");
var loopback = require("loopback");
var app = module.exports = loopback();
var path = require('path');
var dataSource = require('../../Lib/config/datasource.js');
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var generator = require('xoauth2').createXOAuth2Generator({
		user: 'rohit.keshri@indusnet.co.in',
		clientId: '691117601796-d7kpv6c5jnsdn6uatkiq0skfnvvhe872.apps.googleusercontent.com',
		clientSecret: 'xi9KeZuyt-l-usnGm4oyZx0L',
		refreshToken: '1/-0StFrbpgDaspXttVjDm57Yym7sQQToekWMgEcxAjmU',
		accessToken: 'ya29.Ci82A7BgRN25Hy-wG2mz2lZU4mkHPowHZKvRU0JrhaIXd-LNVCOHQ5sPgggq10ndXw' // optional
});
var transporter = nodemailer.createTransport(({
    service: 'gmail',
    auth: {
        xoauth2: generator
    }
}));
module.exports = function(Mail) {

  Mail.sendEmail = function(req,cb) {
    var Client=app.loopback.findModel('client');
    Client.attachTo(dataSource.Datasource);
    app.model(Client);
    console.log("Request :"+req.body);
    Client.find({where:{id:req.body.id}},function(err,client){
      console.log("-------------------------------------------"+JSON.stringify(client));
      var html=getTemplate(client[0],req.body.type,req.body.optional);
      var subject=getSubject(req.body.type);
      if(html){
        transporter.sendMail({
            from: 'rohitkeshri7t7@gmail.com',
            to:  client[0].email,
            subject: subject,
            html: html
        }, function(error, response) {
           if (error) {
                console.log(error);
           } else {
                console.log('Message sent');
                cb(error);
           }
        });
        // Mail.send({
        //   to: client[0].email,
        //   from: 'rohit.keshri@indusnet.co.in',
        //   subject: subject,
        //   text: 'my text',
        //   html: html
        // }, function(err, mail) {
        //   console.log('email sent!'+JSON.stringify(mail));
        //   cb(err);
        // });
      }
    });
  }
  Mail.resetPasswordMail = function(req,cb) {
    var url = 'http://localhost:' + 3000 + '/reset-password';
    var html = 'Click <a href="' + url + '?access_token=' +req.body.token + '">here</a> to reset your password';
          transporter.sendMail({
              from: 'rohit.keshri@indusnet.co.in',
              to:  req.body.email,
              subject: 'Password Reset',
              html: html
          }, function(error, response) {
             if (error) {
                  console.log(error);
             } else {
                  console.log('Message sent');
                  cb(error);
             }
          });
        // Mail.send({
        //   to: req.body.email,
        //   from: 'rohit.keshri@indusnet.co.in',
        //   subject: 'Password Reset',
        //   text: 'my text',
        //   html: html
        // }, function(err, mail) {
        //   console.log('email sent!'+JSON.stringify(mail));
        //   cb(err);
        // });
  }
  Mail.remoteMethod(
    'resetPasswordMail',
    {
      http: {path: '/resetPasswordMail', verb: 'all'},
      returns: {arg: 'status', type: 'object'},
      accepts: {
          arg: 'data',required: true,
          type: 'object',
          http: {
              source: 'req'
          }
      }
    }
  );
  Mail.remoteMethod(
    'sendEmail',
    {
      http: {path: '/sendEmail', verb: 'all'},
      returns: {arg: 'status', type: 'object'},
      accepts: {
          arg: 'data',required: true,
          type: 'object',
          http: {
              source: 'req'
          }
      }
    }
  );
  var getTemplate=function(params,type,optional){
    switch (type) {
        case 'welcome':
        var ascess_level=JSON.parse(params.ascess_level);
            params.cache=ascess_level.cache;
            params.ratelimiter=ascess_level.ratelimiter;
            params.responsetime=ascess_level.responsetime;
            params.optional=optional;
            console.log("pass"+  params.optional);
            var renderer = loopback.template(path.resolve(__dirname, '../../common/view/welcome_template.ejs'));
            html_body = renderer(params);
            return html_body;
            break;
        case 'reminder':
            var validity_to = new Date(params.validity_to);
            var cur_date = new Date();
            var timeDiff = (validity_to.getTime() - cur_date.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
            var reminder=diffDays>0?'Your account expires in '+diffDays+' days.Kindly renew your account before it expired.':'You account validity expired '+Math.abs(diffDays)+' days before.Renew your account before it disabled.';
            params.reminder=reminder;
            var renderer = loopback.template(path.resolve(__dirname, '../../common/view/reminder_template.ejs'));
            html_body = renderer(params);
            return html_body;
            break;
        case 'disable':
            var reminder='Your account is disable . Kindly renew your account before it permanently removed from our system.';
            params.reminder=reminder;
            var renderer = loopback.template(path.resolve(__dirname, '../../common/view/disable_template.ejs'));
            html_body = renderer(params);
            return html_body;
            break;
        case 'update':
        var ascess_level=JSON.parse(params.ascess_level);
            params.cache=ascess_level.cache;
            params.ratelimiter=ascess_level.ratelimiter;
            params.responsetime=ascess_level.responsetime;
            params.changes=optional;
            var renderer = loopback.template(path.resolve(__dirname, '../../common/view/update_template.ejs'));
            html_body = renderer(params);
            return html_body;
            break;
        case 'delete':
            var reminder='Your account is removed permanently from our system.';
            params.reminder=reminder;
            var renderer = loopback.template(path.resolve(__dirname, '../../common/view/delete_template.ejs'));
            html_body = renderer(params);
            return html_body;
            break;
    }
  }
  var getSubject=function(type){
    switch (type) {
        case 'welcome':
            var subject="Welcome To ApiExposer "
            return subject;
            break;
        case 'reminder':
            var subject="Consolidated Renewal Reminder"
            return subject;
            break;
        case 'disable':
            var subject="Consolidated Renewal Reminder"
            return subject;
            break;
        case 'update':
            var subject="Account Update"
            return subject;
            break;
        case 'delete':
            var subject="Account Deleted Permanently"
            return subject;
            break;
    }
  }

};
