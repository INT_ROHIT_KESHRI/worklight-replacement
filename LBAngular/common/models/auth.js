var fs = require('fs');
var replace = require("replace");
var dataSource = require('../../Lib/config/datasource.js');
var uuid = require('uuid');
var loopback = require('loopback');
var app = module.exports = loopback();
var API=app.loopback.findModel('api');
API.attachTo(dataSource.Datasource);
app.model(API);
module.exports = function(Auth) {
  Auth.add=function(req,cb) {
    var instance=req.body;
    API
      .create({
        name: instance.name,
        method: instance.method,
        uri: instance.uri,
        fields: instance.fields,
        modelname:instance.modelname,
        modelid:instance.modelid,
        client_id:instance.client_id,
        content_type:instance.content_type
      },function(err,response){
        if(err) throw err;
          API.create({name:'logout',method:'GET',uri:'logout',fields:JSON.stringify({}),modelname:instance.modelname,modelid:instance.modelid,client_id:instance.client_id,is_logout:1,token:1},function(err,apiresponse){
              response.file_name=instance.file_name;
              response.function_name=instance.function_name;
              response.client_id=instance.client_id;
              response.restrict_login=instance.restrict_login;
              response.max_attempts=instance.max_attempts;
              response.attempt_interval=instance.attempt_interval;
              response.block_time=instance.block_time;
            cb(null,response);
          });

      });
  };
  Auth.remoteMethod(
    'add',
    {
      http: {path: '/users', verb: 'post'},
	    accepts:{ arg: 'data', type: 'object', http: { source: 'req' } },
      returns: {arg: 'status', type: 'string'}
    }
  );
  Auth.afterRemote('add',function(context,remoteMethodOutput,next){
    Auth.create({
      api_id:remoteMethodOutput.status.id,
      model_id:remoteMethodOutput.status.model_id,
      file_name:remoteMethodOutput.status.file_name,
      function_name:remoteMethodOutput.status.function_name,
      client_id:remoteMethodOutput.status.client_id,
      restrict_login:remoteMethodOutput.status.restrict_login,
      max_attempts:remoteMethodOutput.status.max_attempts,
      attempt_interval:remoteMethodOutput.status.attempt_interval,
      block_time:remoteMethodOutput.status.block_time,
      flag:1,
      created:new Date(),
      modified:new Date()
    },function(err,auth){
      if(auth){
         context.res.json('success');
      }else{
         context.res.json('error');
      }
    });
  });

};
