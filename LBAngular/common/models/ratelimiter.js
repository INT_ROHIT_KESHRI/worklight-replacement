var fs = require('fs');
module.exports = function(Ratelimiter) {
  Ratelimiter.observe('before save', function(ctx, next) {
		if(ctx.isNewInstance){
			ctx.instance.created=new Date();
			ctx.instance.modified=new Date();
      ctx.instance.duration=ctx.instance.time*1000;
			next();
		} else{
      if(ctx.data.statuschange==1){
        ctx.data.modified=new Date();
        ctx.data.flag=ctx.data.flag;
  			next();
      }else{
        ctx.data.modified=new Date();
        ctx.data.duration=ctx.data.expiretime*1000;
  			next();
      }

		}
	});
  /*Ratelimiter.observe('after save', function(ctx, next) {
		if(ctx.isNewInstance==false){
      fs.appendFile('../LBServer/log.txt', 'data to append', function (err) {
      });
			next();
		}else{
      next();
    }
	});*/
};
