var fs = require('fs');
module.exports = function(Api) {
	Api.observe('before save', function(ctx, next) {
		  var instance;
			if(ctx.isNewInstance){
				instance=ctx.instance;
				instance.created=new Date();
				instance.modified=new Date();
				instance.flag=1;
				instance.model_id=instance.modelid;
				instance.url="/api/"+instance.modelname+"/"+instance.name;
			}else{
				instance=ctx.data;
				if(instance.attribute){
					instance.url=instance.url;
					instance.modified=new Date();
					next();
					return false;
				}
				if(instance.statuschange){
					instance.modified=new Date();
					next();
					return false;
				}
				console.log("Instance :"+JSON.stringify(instance));
				instance.url='/api/'+instance.modelname+"/"+instance.name
				instance.model_id=instance.model_id;
				instance.modified=new Date();
			}
				var	params=JSON.parse(instance.fields);
				var i;
				var allfields = Object.keys(params).map(function(key,value) {
						return params[value]=key+"!!"+params[key];
				});
				var fields={};
				for(i=0;i<allfields.length;i++){
					fields[i]=allfields[i];
				}
				instance.fields=JSON.stringify(fields);
				next();
	});

	Api.observe('after save', function(ctx, next) {
	/*	fs.appendFile('../LBServer/log.txt', 'data to append', function (err) {
	});*/
		next();

	});


};
