var loopback = require("loopback");
var app = module.exports = loopback();
var Model=app.loopback.findModel('model');
app.model(Model);
var google=require("googleapis");
var request = require("request");
var key={
		client_email:"newserviceaccountga@konvergence-1470405467114.iam.gserviceaccount.com",
    private_key: "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDfoYVxzb0yl77P\nAhwnmZ5nG9mCTi4aO4RW9MUpiE6/rHdSIF0ES1kmaC+uLGyDq79FwzoT9efui6Uk\nyiod7wtmsHWnsNo0b1LXjLkXmM/dVz8L7L/3vcWaN50YVXN4B3StzLrOlmTxgonN\nD9abKcimA6lFlDYdrXf9Flow5WGtKr3cN/IwEBq2NZbxn0CXtoJ7TTaTWHwnfKxs\n7f85KHKDNacNpH9LnOfo1quh5CgtjNFJziORTUxPWmxWf1Xi1S+lIIEyaRTl6+4P\nPopKWIF1awkIzrwer7L0f5jrNRDGpwvHF6OWOmRVvlFsxYivI9HBNbrk+71RnDA8\ndcydHSi5AgMBAAECggEACqA4IQIvnTnMnTVydQc9mZSfLyvb0O9HCIK045ZQd5cQ\nTFyxTsTtxX9p1JLTzSXZ/syZ89zZWmJURH0+pxF2rzhvPrqOg7NOcmODX/Wo2tGM\nc1qafIEN5JgD5OYaiHeCOcfikHJg9YDIX3ADOHgxyoGeB2pVb++QJfxr6Krdi0bH\n5QuJgMLN6k9MlklC/NGijmP9TMC8BK0y/02bybg4SYftt/BUIDAX1RbQZ7rF8kSb\nbmrfJwJ8NKiaX6O28ymMHuuvUI95oXqjiV1LuqdQwX2AIGP1MalvnUwQGK9fWyWJ\nvO4jbvib6+hg51l70jRSaY51tU4sUmKU4kku6TGcAQKBgQDy+LU5VTzs2irzdHMK\nOL4R8VnAe7q7BMeu/CVWHWIp3C8Ba4/QYCVqGclAfYx37ddn4P4rNtJ7GJAQaNgg\nXVGAgIq4wOdnthoRep9yHi/S8ncE5CXcWuP/fZy1Zv4ZgDuIqWGz83iaMAgI+57x\n5sni3IQBdUvozufm6kGfeuNJeQKBgQDrn1LUyA5QddZxtEPyycqePsRMbut2C2yC\nFc95+b09fhFlWzZiQhjy8TLIfLP0O417k18216HZV0dlm+4GbPmhn0Ho+mcGY0K7\n3GduVQNoPQ3chvmjSj0LiFdY2Zd58f/o2k8k+wvefiUI1ZQo+f53NNvgQicQej6f\nZ7y1vBRJQQKBgDjwK5fZ78MZhUM0INleXrxZsMy4shOtlRXepOk/TYe9v443pKYS\nQfJRRKZGRaq9I+OGCEocOMr8AJa32x4KgFT6yodtPU1CkW1TLRZTGKX4iAevZ2fy\nLMfsqdKg5h8Xu9vUxb6j6blIVDm2rZLGoLz3LEAjOry97/5fGy3CYI0RAoGAOBbw\nT+jjx3GVv/GKr1QsIKmObyeEc9BaX0kz37R8wROurnMdKhzT9ZlHXoSZDg3TK1S0\nuMhyJdvqCh0pd2wfrMLNnNlnBxVAAJqCILiqG/lUwGd9K8rzZzhomqUJsyTRp6u4\nZXRBqhBbXpQBNhP06ImzwdndlMWU/LtkJ9sC9UECgYEAsPsC4fiQWtGNvy+yNrd5\nAZNqwf0tTVIMeRaFTFRIbZN7Ah5A/M1RAFCKrpJpwEzb6LvdGACyZrvx4S3hXIST\nvUPJVrtARnoeR5gx0aYoOKfyttV4XmUrfKd1ZKHnWfVLbKKd63PGsJNq6ThbHhGf\nLA8IFwLZY4iG0RiANwLdF/A=\n-----END PRIVATE KEY-----\n"
  };
var scopes = [
	  //  'https://www.googleapis.com/auth/plus.me',
    'https://www.googleapis.com/auth/analytics.edit',
    'https://www.googleapis.com/auth/analytics.readonly'
	];
module.exports = function(Googleanalytic) {
  		Googleanalytic.gettoken = function(req,cb) {
    		var jwtClient = new google.auth.JWT(key.client_email, null, key.private_key, scopes, null);
      	jwtClient.authorize(function(err, tokens) {
        if (err) {
          cb(null,err);
          return;
        }
				var type=req.body.type;
				var id=req.body.id;
				var group_name=req.body.group_name;

				if(type=='Superadmin'){
					Model.find({where:{group_name:group_name}},function(err,models){
						var Activeusers=0;
						var TotalActiveApps=0;
						var async = require('async');
						var A=function(callback2){
							  setTimeout(function(){
									async.eachSeries(models,function(elementOfArray, callback){
										request({
												uri: "https://www.googleapis.com/analytics/v3/data/realtime?ids=ga:"+elementOfArray.view_id+"&metrics=rt:activeUsers&access_token="+tokens.access_token,
												method: "GET",
												}, function(error, response, body) {
													  var result=JSON.parse(body)
														var activeUsers=(result.rows)?result.rows[0][0]:0;
														var activeApps=(activeUsers==0)?0:1;
														Activeusers=Activeusers+parseInt(activeUsers);
														TotalActiveApps=TotalActiveApps+activeApps;
													});
										  callback();
									 }, function(err){
			               if(err){throw err;}
			             });
									callback2();
	              },10);
						}
						var B=function(){
							setTimeout(function(){
								cb(null,{"Activeusers":Activeusers,"TotalActiveApps":TotalActiveApps});
							},2000);
						}
						A(function(){
							B();
						});
					});
				}else{
					Model.find({where:{group_name:group_name,client_id:id}},function(err,models){
						var Activeusers=0;
						var TotalActiveApps=0;
						var async = require('async');
						var A=function(callback2){
							  setTimeout(function(){
									async.eachSeries(models,function(elementOfArray, callback){
										request({
												uri: "https://www.googleapis.com/analytics/v3/data/realtime?ids=ga:"+elementOfArray.view_id+"&metrics=rt:activeUsers&access_token="+tokens.access_token,
												method: "GET",
												}, function(error, response, body) {
													  var result=JSON.parse(body)
														var activeUsers=(result.rows)?result.rows[0][0]:0;
														var activeApps=(activeUsers==0)?0:1;
														Activeusers=Activeusers+parseInt(activeUsers);
														TotalActiveApps=TotalActiveApps+activeApps;
													});
										  callback();
									 }, function(err){
			               if(err){throw err;}
			             });
									callback2();
	              },10);
						}
						var B=function(){
							setTimeout(function(){
								cb(null,{"Activeusers":Activeusers,"TotalActiveApps":TotalActiveApps});
							},2000);
						}
						A(function(){
							B();
						});
					 });
				}
        });
      }

      Googleanalytic.remoteMethod(
          'gettoken',
          {
            http: {path: '/gettoken', verb: 'post'},
      	    accepts:{ arg: 'data', type: 'object', http: { source: 'req' }},
            returns: {arg: 'status', type: 'string'}
          }
      );
      Googleanalytic.afterRemote('gettoken',function(context,remoteMethodOutput,next){
        context.res.json(remoteMethodOutput.status);
      });






			/* Code to Fetch event data from google analytics */

			Googleanalytic.getchartdata = function(req,cb) {
    		var jwtClient = new google.auth.JWT(key.client_email, null, key.private_key, scopes, null);
      	jwtClient.authorize(function(err, tokens) {
        if (err) {
          cb(null,err);
          return;
        }
				var type=req.body.type;
				var id=req.body.id;
				var group_name=req.body.group_name;
				var start_date=req.body.startdate;
				var end_date=req.body.enddate;
 				//console.log("Start-date::"+start_date);
			//	console.log("End-date::"+end_date);
				if(type=='Superadmin'){
					Model.find({where:{group_name:group_name}},function(err,models){
						var Activeusers=0;
						var TotalActiveApps=0;
						var async = require('async');
						var A=function(callback2){
								setTimeout(function(){
									async.eachSeries(models,function(elementOfArray, callback){
										request({
												uri: "https://www.googleapis.com/analytics/v3/data/realtime?ids=ga:"+elementOfArray.view_id+"&metrics=rt:activeUsers&access_token="+tokens.access_token,
												method: "GET",
												}, function(error, response, body) {
														var result=JSON.parse(body)
														var activeUsers=(result.rows)?result.rows[0][0]:0;
														var activeApps=(activeUsers==0)?0:1;
														Activeusers=Activeusers+parseInt(activeUsers);
														TotalActiveApps=TotalActiveApps+activeApps;
													});
											callback();
									 }, function(err){
										 if(err){throw err;}
									 });
									callback2();
								},10);
						}
						var B=function(){
							setTimeout(function(){
								cb(null,{"Activeusers":Activeusers,"TotalActiveApps":TotalActiveApps});
							},2000);
						}
						A(function(){
							B();
						});
					});
				}else{
					Model.find({where:{group_name:group_name,client_id:id}},function(err,models){
						var events=[];
						var labels=[];
						var dates=[];
						var async = require('async');
						var A=function(callback2){
							  setTimeout(function(){
									async.eachSeries(models,function(elementOfArray, callback){
										request({
												uri: "https://www.googleapis.com/analytics/v3/data/ga?ids=ga:"+elementOfArray.view_id+"&start-date="+start_date+"&end-date="+end_date+"&metrics=ga:sessions&dimensions=ga:eventCategory,ga:eventLabel,ga:date&include-empty-rows=false&output=json&samplingLevel=HIGHER_PRECISION&access_token="+tokens.access_token,
												method: "GET",
												}, function(error, response, body) {

													  var result=JSON.parse(body);
														console.log(result);
														var i;
														if(result.rows){
															for(i=0;i<result.rows.length;i++){
																events.push(result.rows[i][0]);
																labels.push(result.rows[i][1]);
																dates.push(result.rows[i][2]);
															}
														}
													});
										  callback();
									 }, function(err){
			               if(err){throw err;}
			             });
									callback2();
	              },10);
						}
						var B=function(){
							setTimeout(function(){
								cb(null,{"Events":events,"Labels":labels,"Dates":dates});
							},5000);
						}
						A(function(){
							B();
						});
					 });
				}
        });
      }

      Googleanalytic.remoteMethod(
          'getchartdata',
          {
            http: {path: '/getchartdata', verb: 'post'},
      	    accepts:{ arg: 'data', type: 'object', http: { source: 'req' }},
            returns: {arg: 'status', type: 'string'}
          }
      );
      Googleanalytic.afterRemote('getchartdata',function(context,remoteMethodOutput,next){
        context.res.json(remoteMethodOutput.status);
      });
};
