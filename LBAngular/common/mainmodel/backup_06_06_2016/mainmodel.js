var jwt = require('jwt-simple');
var request = require("request");
var uuid = require('node-uuid');
var datetime = require('node-datetime');
var setting = require('../../Lib/config/setting.js');
var validation = require('../../Lib/validation.js');
var encrypt = require('../../Lib/encrypting.js');
var loopback = require('loopback');
var app = module.exports = loopback();
var parseString = require('xml2js').parseString;
var NodeCache = require("node-cache");
var myCache = new NodeCache();
var cachingapi = require('../../Lib/cachingapi.js');
var dataSource = require('../../Lib/config/datasource.js');
var properties = require('../../Lib/config/properties.js');
module.exports = function(ModelName) {
    //app.model('tokensecret', setting.setting);


    app.model('cache', {
        dataSource: dataSource.Datasource,
        properties: properties.Cache
    });
    app.model('apifields', {
        dataSource: dataSource.Datasource,
        properties: properties.Apifields
    });
    var CACHING = app.models.cache;
    var APIFIELDS = app.models.apifields;

    var API_NAME = '';
    var MODEL_API = '';
    var API_METHOD = '';
    var API_URL = '';
    var API_URI = '';
    var API_CACHE = '';
    var API_FIELDS = '';


    var formfields = {};
    ModelName.beforeRemote('**', function(ctx, inst, next) {
        API_NAME = ctx.req.set_api_details.details.name;
        API_METHOD = ctx.req.set_api_details.details.method;
        API_URI = ctx.req.set_api_details.details.api_uri;
        API_URL = ctx.req.set_api_details.details.api_url;
        if (ctx.req.method != API_METHOD)
            ctx.res.json("Undefined Method");
        CACHING.find({
            where: {
                api_id: ctx.req.set_api_details.details.api_id
            }
        }, function(err, result) {
            if (result.length > 0) {
                API_CACHE = result[0];
            } else {
                API_CACHE = null;
            }
        });
        if (API_METHOD == 'POST') {
            APIFIELDS.find({
                where: {
                    api_id: ctx.req.set_api_details.details.api_id
                }
            }, function(err, apifields) {
                var i;
                var fields = JSON.parse(apifields[0].fields);
                var allfields = Object.keys(fields).map(function(k) {
                    return fields[k]
                });
                var fieldname;
                var body = ctx.req.body;
                allfields.forEach(function(fieldname, index) {
                    fieldname = fieldname.split("|");
                    formfields[fieldname[0]] = body[fieldname[0]];
                });
            });
        }
        if (API_CACHE != null) {
            try {
                cachvalue = myCache.get(API_CACHE.cachkey);
                if (cachvalue == undefined) {
                    console.log('The value is ' + cachvalue);
                    next();
                } else {
                    ctx.res.json(cachvalue);
                }
            } catch (err) {
                console.log(err);
                next();
            }
        } else {
            next();
        }
    });
    ModelName[API_NAME] = function(req, cb) {
        if (API_METHOD == 'POST') {
            setTimeout(load, 100);

            function load() {
                request({
                    uri: API_URI,
                    method: API_METHOD,
                    form: formfields
                }, function(error, response, body) {
                    cb(null, body);
                });
            }
        } else {
            request({
                uri: API_URI,
                method: API_METHOD
            }, function(error, response, body) {
                cb(null, body);
            });
        }
    }
    ModelName.afterRemote('**', function(context, remoteMethodOutput, next) {
        setCaching(API_CACHE, remoteMethodOutput.status);
        context.res.send(remoteMethodOutput.status);

    });



    ModelName.remoteMethod(
        API_NAME, {
            http: {
                verb: "GET",
                path: '/:api'
            },
            accepts: {
                arg: 'data',
                type: 'object',
                http: {
                    source: 'req'
                }
            },
            returns: {
                arg: 'status',
                type: 'String'
            }
        }
    );
    ModelName.remoteMethod(
        API_NAME, {
            http: {
                verb: 'POST',
                path: '/:api'
            },
            accepts: {
                arg: 'data',
                type: 'object',
                http: {
                    source: 'req'
                }
            },
            returns: {
                arg: 'status',
                type: 'String'
            }
        }
    );

    var setCaching = function(cachedetails, data) {
        if (cachedetails != null) {
            output = data;
            cachkey = cachedetails.cachkey;
            duration = cachedetails.duration;
            myCache.set(cachkey, output, duration);
        }
    }


};
