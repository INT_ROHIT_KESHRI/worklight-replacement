var jwt = require('jwt-simple');
var request = require("request");
var uuid = require('node-uuid');
var setting = require('../../Lib/config/setting.js');
var Statuscode = require('../../Lib/statuscode.js');
var loopback = require('loopback');
var app = module.exports = loopback();
var parseString = require('xml2js').parseString;
var NodeCache = require("node-cache");
var myCache = new NodeCache();
var dataSource = require('../../Lib/config/datasource.js');
var properties = require('../../Lib/config/properties.js');
var ua = require('universal-analytics');
module.exports = function(ModelName) {
    /* Declaring the model */
    app.model('tokensecret', setting.setting);
    app.model('cache', {
        dataSource: dataSource.Datasource,
        properties: properties.Cache
    });
    app.model('auth', {
        dataSource: dataSource.Datasource,
        properties: properties.Auth
    });
    /* End */
    /* Initailizing the models */
    var CACHING = app.models.cache;
    var AUTH = app.models.auth;
    var TOKENSECRET = app.models.tokensecret;
    /* End */
    var API_NAME = '';
    var API_METHOD = '';
    var API_URL = '';
    var API_URI = '';
    var API_CACHE = null;
    var FORM_FIELDS = '';
    var GENERATE_AUTHTOKEN = false;
    var LOGIN_STATUS_STRING = '';
    var LOGIN_SUCCESS_CODE = '';
    var LOGIN_FAILURE_CODE = '';
    var LOGIN_PRIMARY_ID = '';
    var IS_LOGIN_API=false;
    var google_analytics_params={};
    var visitor;
    ModelName.beforeRemote('**', function(ctx, inst, next) {
        if (!ctx.req.cookies.cookiename) {
            ctx.res.cookie('cookiename', uuid.v1(), {
                maxAge: 20000000,
                httpOnly: true
            });
        }
        API_NAME = ctx.req.set_api_details.details.api_name;
        API_METHOD = ctx.req.set_api_details.details.method;
        API_URI = ctx.req.set_api_details.details.api_uri;
        API_URL = ctx.req.set_api_details.details.url;
        API_PARAMS = ctx.req.set_api_details.details.api_params;
        API_ID = ctx.req.set_api_details.details.api_id;
        MODEL_NAME = ctx.req.set_api_details.details.model_name;
        ASCESS_TOKEN_STATUS = ctx.req.set_api_details.details.api_token_status;
        /* Set Google Analytics data Start */
        visitor = ua(ctx.req.set_api_details.details.tracking_id,{https: true});
            google_analytics_params.ec=API_URL;
            google_analytics_params.ea=API_NAME;
            google_analytics_params.cid=ctx.req.sessionID;
            google_analytics_params.ev=1;
        /* End */
        if (ctx.req.set_api_details.details.api_fields != '' && API_METHOD == 'POST') {
            FORM_FIELDS = ctx.req.post_fields.details;
            AUTH.find({
                where: {
                    api_id: API_ID,
                    flag: 1
                }
            }, function(err, log) {
                if (log.length > 0) {
                    LOGIN_STATUS_STRING=log[0].status_string,
                    LOGIN_SUCCESS_CODE=log[0].status_success_code,
                    LOGIN_FAILURE_CODE=log[0].status_failure_code,
                    LOGIN_PRIMARY_ID=log[0].primary_id
                    GENERATE_AUTHTOKEN = true;
                } else {
                    GENERATE_AUTHTOKEN = false;
                    LOGIN_STATUS_STRING = '';
                    LOGIN_SUCCESS_CODE = '';
                    LOGIN_FAILURE_CODE = '';
                    LOGIN_PRIMARY_ID='';
                }
            });
        } else {
            IS_LOGIN_API=false;
            GENERATE_AUTHTOKEN = false;
            LOGIN_STATUS_STRING = '';
            LOGIN_SUCCESS_CODE = '';
            LOGIN_FAILURE_CODE = '';
            LOGIN_PRIMARY_ID='';
        }
        CACHING.find({
            where: {
                api_id: ctx.req.set_api_details.details.api_id,
                flag:1
            }
        }, function(err, result) {
            if (result.length > 0) {
                API_CACHE = result[0];
                API_CACHE.cookievalue = ctx.req.cookies.cookiename;
                cachvalue = myCache.get(API_CACHE.cachkey + API_CACHE.cookievalue);
                if (cachvalue == undefined) {
                    next();
                } else {
                    Statuscode.Status[200].data=cachvalue;
                    google_analytics_params.el="Success";
                    visitor.event(google_analytics_params).send();
                    ctx.res.json(Statuscode.Status[200]);
                    delete Statuscode.Status[200].data;
                }
            } else {
                API_CACHE = null;
                next();
            }
        });
    });
    ModelName[API_NAME] = function(req, cb) {
        if (API_METHOD == 'POST') {
            request({
                uri: API_URI,
                method: API_METHOD,
                form: FORM_FIELDS
            }, function(error, response, body) {
                if(LOGIN_STATUS_STRING!=''){
                  if(response[LOGIN_STATUS_STRING]==LOGIN_FAILURE_CODE){
                    google_analytics_params.el="Failed";
                    visitor.event(google_analytics_params).send();
                    IS_LOGIN_API=true;
                    GENERATE_AUTHTOKEN=false;
                  }else{
                    IS_LOGIN_API=false;
                    google_analytics_params.el="Success";
                    visitor.event(google_analytics_params).send();
                  }
                }else{
                  IS_LOGIN_API=false;
                }
                if (body == undefined) {
                    cb(null, {
                        "Response": body,
                        "Error": 1
                    });
                } else {
                    jsonindex = response.headers['content-type'].split("/").indexOf("json");
                    xmlindex = response.headers['content-type'].split("/");
                    xmlindex = xmlindex[1].indexOf("xml");
                    if (xmlindex == -1 && jsonindex == -1) cb(null, {
                        "Response": body,
                        "Error": 1
                    });
                    cb(null, {
                        "Response": body,
                        "type": jsonindex
                    });
                }
            });
        } else if(API_METHOD == 'GET'){
            request({
                uri: API_URI + API_PARAMS,
                method: API_METHOD
            }, function(error, response, body) {
                if (body == undefined) {
                    cb(null, {
                        "Response": body,
                        "Error": 1
                    });
                } else {
                    jsonindex = response.headers['content-type'].split("/").indexOf("json");
                    xmlindex = response.headers['content-type'].split("/");
                    xmlindex = xmlindex[1].indexOf("xml");
                    if (xmlindex == -1 && jsonindex == -1) cb(null, {
                        "Response": body,
                        "Error": 1
                    });
                    cb(null, {
                        "Response": body,
                        "type": jsonindex
                    });
                }
            });
        } else if(API_METHOD == 'DELETE'){
            request({
                uri:API_URI + API_PARAMS,
                method:API_METHOD
            }, function(error, response, body) {
                if(body==undefined) {
                  cb(null, {
                      "Response": body,
                      "Error": 1
                  });
                } else{
                  jsonindex = response.headers['content-type'].split("/").indexOf("json");
                  xmlindex = response.headers['content-type'].split("/");
                  xmlindex = xmlindex[1].indexOf("xml");
                  if (xmlindex == -1 && jsonindex == -1) cb(null, {
                      "Response": body,
                      "Error": 1
                  });
                  cb(null, {
                      "Response": body,
                      "type": jsonindex
                  });
                }
            });
        }
    }
    ModelName.afterRemote('**', function(context, remoteMethodOutput, next) {
        var token = (context.req.body && context.req.body.access_token) || (context.req.query && context.req.query.access_token) || context.req.headers['x-access-token'];
        var LBinstanceId = (context.req.body && context.req.body.lbinstance_id) || context.req.headers['lbinstance_id'];
        var response;
        if (remoteMethodOutput.status.Error){
            google_analytics_params.el="Failed";
            visitor.event(google_analytics_params).send();
            context.res.json(Statuscode.Status[419]);
        }
        if (remoteMethodOutput.status.type > -1) {
            response = remoteMethodOutput.status.Response;
        } else {
            parseString(remoteMethodOutput.status.Response, {
                trim: false,
                explicitArray: false,
                explicitRoot: false
            }, function(err, result) {
                response = result;
            });
        }

        if (GENERATE_AUTHTOKEN) {
            var json = response;
            var js = JSON.parse(json);
            var id = parseInt(getValues(js, LOGIN_PRIMARY_ID));
            var ret = new Date();
            var expiretime = ret.setTime(ret.getTime() + 10 * 60000);
            var created = new Date();
            if (!token) {
                var ret = new Date();
                var token = jwt.encode({
                    iss: id,
                    exp: expiretime
                }, 'jwtTokenSecret');
                var data = {
                    'user_id': id,
                    'lbinstance_id': LBinstanceId,
                    'tokensecret': token,
                    'service': MODEL_NAME,
                    'expiretime': expiretime,
                    'created': created,
                    'updated': created
                };
                TOKENSECRET.findOrCreate({
                    where: {
                        user_id: id,
                        service: MODEL_NAME
                    }
                }, data, function(err, instance, created) {
                    if (created == false) {
                        TOKENSECRET.updateAll({
                            id: instance.id
                        }, {
                            tokensecret: '',
                            user_id: 0
                        }, function(err, info) {
                            TOKENSECRET.create(data, function(err, tokensecret) {
                                TOKENSECRET.destroyAll({
                                    user_id: 0,
                                    lbinstance_id: LBinstanceId
                                }, function(err, response) {});
                            });
                        });
                    } else {
                        TOKENSECRET.destroyAll({
                            user_id: 0,
                            lbinstance_id: LBinstanceId
                        }, function(err, response) {});
                    }
                });
                Statuscode.Status[200]['x-access-token']=token;
                google_analytics_params.el="Success";
                visitor.event(google_analytics_params).send();
                context.res.json(Statuscode.Status[200]);
                delete Statuscode.Status[200]['x-access-token'];
            } else {
                Statuscode.Status[200].data=response;
                google_analytics_params.el="Success";
                visitor.event(google_analytics_params).send();
                context.res.json(Statuscode.Status[200]);
                delete Statuscode.Status[200].data;
            }
        } else {
            if (ASCESS_TOKEN_STATUS == 1 && !token) {
                context.res.json(Statuscode.Status['401']);
            } else {
                if(IS_LOGIN_API==false){
                  google_analytics_params.el="Success";
                  visitor.event(google_analytics_params).send();
                }
                setCaching(API_CACHE, response);
                Statuscode.Status[200].data=response;
                context.res.json(Statuscode.Status[200]);
                delete Statuscode.Status[200].data;
            }

        }

    });

    ModelName.remoteMethod(
        API_NAME, {
            http: {
                verb: "GET",
                path: '/:api'
            },
            accepts: {
                arg: 'data',
                type: 'object',
                http: {
                    source: 'req'
                }
            },
            returns: {
                arg: 'status',
                type: 'String'
            }
        }
    );
    ModelName.remoteMethod(
        API_NAME, {
            http: {
                verb: 'POST',
                path: '/:api'
            },
            accepts: {
                arg: 'data',
                type: 'object',
                http: {
                    source: 'req'
                }
            },
            returns: {
                arg: 'status',
                type: 'String'
            }
        }
    );
    ModelName.remoteMethod(
        API_NAME, {
            http: {
                verb: 'DEL',
                path: '/:api'
            },
            accepts: {
                arg: 'data',
                type: 'object',
                http: {
                    source: 'req'
                }
            },
            returns: {
                arg: 'status',
                type: 'String'
            }
        }
    );

    var setCaching = function(cachedetails, data) {
        if (cachedetails != null) {
            output = data;
            cachkey = cachedetails.cachkey + cachedetails.cookievalue;
            duration = cachedetails.duration;
            myCache.set(cachkey, output, duration);
            return true;
        } else {
            return false;
        }
    }
    var getValues = function(obj, key) {
        var objects = [];
        for (var i in obj) {
            if (!obj.hasOwnProperty(i)) continue;
            if (typeof obj[i] == 'object') {
                objects = objects.concat(getValues(obj[i], key));
            } else if (i == key) {
                objects.push(obj[i]);
            }
        }
        return objects;
    }

};
