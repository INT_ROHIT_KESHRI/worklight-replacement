var jwt = require('jwt-simple');
var request = require("request");
var uuid = require('node-uuid');
var setting = require('../../Lib/config/setting.js');
var Statuscode = require('../../Lib/statuscode.js');
var loopback = require('loopback');
var app = module.exports = loopback();
var parseString = require('xml2js').parseString;
var NodeCache = require("node-cache");
var myCache = new NodeCache();
var dataSource = require('../../Lib/config/datasource.js');
var properties = require('../../Lib/config/properties.js');
var ua = require('universal-analytics');
const {
    NodeVM
} = require('vm2');
var fs = require("fs");
const vm = new NodeVM({
    console: 'inherit',
    sandbox: {}
});
module.exports = function(ModelName) {
    /* Declaring the model */
    app.model('tokensecret', setting.setting);
    app.model('cache', {
        dataSource: dataSource.Datasource,
        properties: properties.Cache
    });
    app.model('auth', {
        dataSource: dataSource.Datasource,
        properties: properties.Auth
    });
    app.model('logincounter', {
        dataSource: dataSource.Datasource,
        properties: properties.Logincounter
    });
    /* End */
    /* Initailizing the models */
    var CACHING = app.models.cache;
    var AUTH = app.models.auth;
    var TOKENSECRET = app.models.tokensecret;
    var LOGINCOUNTER = app.models.logincounter;
    /* End */
    var API_NAME = '';
    var API_METHOD = '';
    var API_URL = '';
    var API_URI = '';
    var API_CACHE = null;
    var FORM_FIELDS = '';
    var GENERATE_AUTHTOKEN = false;
    var AUTH_FILE_NAME = '';
    var AUTH_FUNCTION_NAME = '';
    var IS_LOGIN_API = false;
    var LOGIN_ATTEMPT_DIFFRENCE = '';
    var MODEL_ID = '';
    var IS_RESTRICT_LOGIN = 0;
    var CONTENT_TYPE;
    var XML_POST_DATA;
    var google_analytics_params = {};
    var visitor;
    ModelName.beforeRemote('**', function(ctx, inst, next) {
        if (!ctx.req.cookies.cookiename) {
            ctx.res.cookie('cookiename', uuid.v1(), {
                maxAge: 20000000000,
                httpOnly: true
            });
        }
        /* Declaring all the variables and Initialize google analytics in function A */
        var A = function(callback) {
            API_NAME = ctx.req.set_api_details.details.api_name;
            API_METHOD = ctx.req.set_api_details.details.method;
            API_URI = ctx.req.set_api_details.details.api_uri;
            API_URL = ctx.req.set_api_details.details.url;
            API_PARAMS = ctx.req.set_api_details.details.api_params;
            API_ID = ctx.req.set_api_details.details.api_id;
            MODEL_NAME = ctx.req.set_api_details.details.model_name;
            ASCESS_TOKEN_STATUS = ctx.req.set_api_details.details.api_token_status;
            FORM_FIELDS = ctx.req.post_fields.details;
            MODEL_ID = ctx.req.set_api_details.details.model_id;
            CONTENT_TYPE=ctx.req.set_api_details.details.content_type;
            if(CONTENT_TYPE=='xml' && FORM_FIELDS!='' && (API_METHOD=='POST'||API_METHOD=='PUT')){
            parseString(JSON.stringify(FORM_FIELDS), {
                trim: false,
                explicitArray: false
            }, function(err, result) {
                XML_POST_DATA=result
            });
            }else{
                  XML_POST_DATA='';
            }
            /* Set Google Analytics data Start */
            visitor = ua(ctx.req.set_api_details.details.tracking_id, {
                https: true
            });
            google_analytics_params.ec = API_URL;
            google_analytics_params.ea = API_NAME;
            google_analytics_params.cid = ctx.req.sessionID;
            google_analytics_params.ev = 1;
            /* End */
            /* Check for Maximum attempt to login */
            if (ctx.req.set_api_details.details.api_fields != '' && ctx.req.set_api_details.details.method == 'POST' && ctx.req.cookies.cookiename) {
                check_login_counter(ctx, function() {
                    callback();
                })
            } else {
                callback();
            }
            /* End */

        }
        var B = function(callback2) {
            if (ctx.req.set_api_details.details.api_fields != '' && API_METHOD == 'POST') {
                AUTH.find({
                    where: {
                        api_id: API_ID,
                        flag: 1
                    }
                }, function(err, log) {
                    if (log.length > 0) {
                        AUTH_FILE_NAME = log[0].file_name;
                        AUTH_FUNCTION_NAME = log[0].function_name;
                        LOGIN_ATTEMPT_DIFFRENCE = log[0].attempt_interval * 60;
                        GENERATE_AUTHTOKEN = true;
                        IS_LOGIN_API = true;
                        IS_RESTRICT_LOGIN = log[0].restrict_login;
                    } else {
                        GENERATE_AUTHTOKEN = false;
                        AUTH_FILE_NAME = '',
                            AUTH_FUNCTION_NAME = '',
                            IS_LOGIN_API = false;
                        IS_RESTRICT_LOGIN = 0;
                    }
                });
            } else {
                IS_LOGIN_API = false;
                GENERATE_AUTHTOKEN = false;
                AUTH_FILE_NAME = '',
                    AUTH_FUNCTION_NAME = ''
            }
            callback2();
        }
        A(function() {
            B(function() {
                CACHING.find({
                    where: {
                        api_id: ctx.req.set_api_details.details.api_id,
                        flag: 1
                    }
                }, function(err, result) {
                    if (result.length > 0) {
                        API_CACHE = result[0];
                        API_CACHE.cookievalue = ctx.req.cookies.cookiename;
                        cachvalue = myCache.get(API_CACHE.cachkey + API_CACHE.cookievalue);
                        if (cachvalue == undefined) {
                            next();
                        } else {
                            Statuscode.Status[200].data = cachvalue;
                            google_analytics_params.el = "Success";
                            visitor.event(google_analytics_params).send();
                            ctx.res.json(Statuscode.Status[200]);
                            delete Statuscode.Status[200].data;
                        }
                    } else {
                        API_CACHE = null;
                        next();
                    }
                });
            });
        });
    });
    ModelName[API_NAME] = function(req, cb) {
        if (API_METHOD == 'POST') {
            request({
              uri: API_URI,
              method: API_METHOD,
              contentType:(CONTENT_TYPE=='json')?"application/json":(CONTENT_TYPE=='xml')?"text/xml":"",
              form: (CONTENT_TYPE=='form')?FORM_FIELDS:"",
              body:(CONTENT_TYPE=='json')?JSON.stringify(FORM_FIELDS):"",
              data:(CONTENT_TYPE=='xml')?XML_POST_DATA:""
            }, function(error, response, body) {
                if (response == undefined) {
                    cb(null, {
                        "Response": body,
                        "Error": 1
                    });
                } else {
                    check_content_type(response.headers['content-type'], function(xmlindex) {
                        if (IS_LOGIN_API == true) {
                            /* Call the js file uploaded by client */
                            fs.readFile('../LBAngular/server/storage/authjsfiles/' + AUTH_FILE_NAME + '.js', function(err, fd) {
                                if (err) {
                                    return console.error(err);
                                }
                                let AUTH_FUNCTION_NAME = vm.run(fd.toString());
                                AUTH_FUNCTION_NAME(response, function(success) {
                                    google_analytics_params.el = "Success";
                                    visitor.event(google_analytics_params).send();
                                    cb(null, {
                                        "Response": success,
                                        "type": xmlindex
                                    });
                                }, function(error) {
                                    /* Section for blocking the login api after given n numbers of  wrong credentials */
                                    if (req.cookies.cookiename && IS_RESTRICT_LOGIN == 1) {
                                        LOGINCOUNTER.find({
                                            where: {
                                                cookie: req.cookies.cookiename,
                                                model_id: MODEL_ID
                                            }
                                        }, function(err, result) {
                                            if (result.length > 0) {
                                                var current_date = new Date();
                                                var lastattempt = new Date(result[0].updated);
                                                var timeDiff = current_date.getTime() - lastattempt.getTime();
                                                var diffsecs = Math.ceil(timeDiff / 1000);
                                                var counter = result[0].counter;
                                                if (diffsecs > LOGIN_ATTEMPT_DIFFRENCE) {
                                                    LOGINCOUNTER.updateAll({
                                                        cookie: req.cookies.cookiename,
                                                        model_id: MODEL_ID
                                                    }, {
                                                        counter: 1,
                                                        updated: new Date()
                                                    }, function(err, info) {});
                                                } else {
                                                    LOGINCOUNTER.updateAll({
                                                        cookie: req.cookies.cookiename,
                                                        model_id: MODEL_ID
                                                    }, {
                                                        counter: counter + 1
                                                    }, function(err, info) {});
                                                }
                                            } else {
                                                LOGINCOUNTER.create({
                                                    cookie: req.cookies.cookiename,
                                                    counter: 1,
                                                    model_id: MODEL_ID,
                                                    created: new Date(),
                                                    updated: new Date()
                                                }, function(err, obj) {});
                                            }
                                        });
                                    }
                                    /* End */
                                    google_analytics_params.el = "Failed";
                                    visitor.event(google_analytics_params).send();
                                    GENERATE_AUTHTOKEN = false;
                                    cb(null, {
                                        "Response": error,
                                        "type": xmlindex
                                    });
                                });
                            });
                            /*End*/
                        } else {
                            cb(null, {
                                "Response": body,
                                "type": xmlindex
                            });
                        }
                    });
                }
                /*End */
            });
        } else if (API_METHOD == 'GET') {
            request({
                uri: API_URI + API_PARAMS,
                method: API_METHOD
            }, function(error, response, body) {
                if (response == undefined) {
                    cb(null, {
                        "Response": body,
                        "Error": 1
                    });
                } else {
                    check_content_type(response.headers['content-type'], function(xmlindex) {
                        cb(null, {
                            "Response": body,
                            "type": xmlindex
                        });
                    });
                }
            });
        } else if (API_METHOD == 'DELETE') {
            request({
                uri: API_URI + API_PARAMS,
                method: API_METHOD
            }, function(error, response, body) {
                if (body == undefined) {
                    cb(null, {
                        "Response": body,
                        "Error": 1
                    });
                } else {
                    check_content_type(response.headers['content-type'], function(xmlindex) {
                        cb(null, {
                            "Response": body,
                            "type": xmlindex
                        });
                    });
                }
            });
        }
    }
    ModelName.afterRemote('**', function(context, remoteMethodOutput, next) {
        var token = (context.req.body && context.req.body.access_token) || (context.req.query && context.req.query.access_token) || context.req.headers['x-access-token'];
        var LBinstanceId = (context.req.body && context.req.body.lbinstance_id) || context.req.headers['lbinstance_id'];
        var response;
        if (remoteMethodOutput.status.Error) {
            google_analytics_params.el = "Failed";
            visitor.event(google_analytics_params).send();
            context.res.json(Statuscode.Status[419]);
        } else if (remoteMethodOutput.status.type == -1) {
            response = remoteMethodOutput.status.Response;
        } else {
            parseString(remoteMethodOutput.status.Response, {
                trim: false,
                explicitArray: false,
                explicitRoot: false
            }, function(err, result) {
                response = result;
            });
        }
        if (GENERATE_AUTHTOKEN) {
            var json = response;
            var salt = "loopback";
            var user_unique_identity = encript(context.req.post_fields.details, salt);
            var ret = new Date();
            var expiretime = ret.setTime(ret.getTime() + 10 * 60000);
            var created = new Date();
            if (!token) {
                var ret = new Date();
                var token = jwt.encode({
                    iss: user_unique_identity,
                    exp: expiretime
                }, 'jwtTokenSecret');
                var data = {
                    'user_unique_identity': user_unique_identity,
                    'lbinstance_id': LBinstanceId,
                    'tokensecret': token,
                    'service': MODEL_NAME,
                    'expiretime': expiretime,
                    'created': created,
                    'updated': created
                };
                TOKENSECRET.find({
                    where: {
                        user_unique_identity: user_unique_identity,
                        service: MODEL_NAME
                    }
                }, function(err, result) {});
                TOKENSECRET.findOrCreate({
                    where: {
                        user_unique_identity: user_unique_identity,
                        service: MODEL_NAME
                    }
                }, data, function(err, instance, created) {
                    if (created == false) {
                        TOKENSECRET.updateAll({
                            id: instance.id
                        }, {
                            tokensecret: '',
                            user_unique_identity: ''
                        }, function(err, info) {
                            TOKENSECRET.create(data, function(err, tokensecret) {
                                TOKENSECRET.destroyAll({
                                    user_unique_identity: '',
                                    lbinstance_id: LBinstanceId
                                }, function(err, response) {});
                            });
                        });
                    } else {
                        TOKENSECRET.destroyAll({
                            user_unique_identity: '',
                            lbinstance_id: LBinstanceId
                        }, function(err, response) {});
                    }
                });
                Statuscode.Status[200]['x-access-token'] = token;
                google_analytics_params.el = "Success";
                visitor.event(google_analytics_params).send();
                context.res.json(Statuscode.Status[200]);
                delete Statuscode.Status[200]['x-access-token'];
            } else {
                Statuscode.Status[200].data = response;
                google_analytics_params.el = "Success";
                visitor.event(google_analytics_params).send();
                context.res.json(Statuscode.Status[200]);
                delete Statuscode.Status[200].data;
            }
        } else {
            if (ASCESS_TOKEN_STATUS == 1 && !token) {
                context.res.json(Statuscode.Status['401']);
            } else {
                if (IS_LOGIN_API == false) {
                    google_analytics_params.el = "Success";
                    visitor.event(google_analytics_params).send();
                }
                setCaching(API_CACHE, response);
                Statuscode.Status[200].data = response;
                context.res.json(Statuscode.Status[200]);
                delete Statuscode.Status[200].data;
            }
        }
    });

    ModelName.remoteMethod(
        API_NAME, {
            http: {
                verb: "GET",
                path: '/:api'
            },
            accepts: {
                arg: 'data',
                type: 'object',
                http: {
                    source: 'req'
                }
            },
            returns: {
                arg: 'status',
                type: 'String'
            }
        }
    );
    ModelName.remoteMethod(
        API_NAME, {
            http: {
                verb: 'POST',
                path: '/:api'
            },
            accepts: {
                arg: 'data',
                type: 'object',
                http: {
                    source: 'req'
                }
            },
            returns: {
                arg: 'status',
                type: 'String'
            }
        }
    );
    ModelName.remoteMethod(
        API_NAME, {
            http: {
                verb: 'DEL',
                path: '/:api'
            },
            accepts: {
                arg: 'data',
                type: 'object',
                http: {
                    source: 'req'
                }
            },
            returns: {
                arg: 'status',
                type: 'String'
            }
        }
    );

    var setCaching = function(cachedetails, data) {
        if (cachedetails != null) {
            output = data;
            cachkey = cachedetails.cachkey + cachedetails.cookievalue;
            duration = cachedetails.duration;
            myCache.set(cachkey, output, duration);
            return true;
        } else {
            return false;
        }
    }
    var encript = function(o, salt) {
        o = JSON.stringify(o).split('');
        for (var i = 0, l = o.length; i < l; i++)
            if (o[i] == '{')
                o[i] = '}';
            else if (o[i] == '}')
            o[i] = '{';
        return encodeURI(salt + o.join(''));
    }

    var check_login_counter = function(ctx, callback) {
        AUTH.find({
            where: {
                api_id: ctx.req.set_api_details.details.api_id,
                flag: 1,
                restrict_login: 1,
            }
        }, function(err, log) {
            if (log.length > 0) {
                var Interval = log[0].attempt_interval * 60;
                var Block_time = log[0].block_time * 60;
                LOGINCOUNTER.find({
                    where: {
                        cookie: ctx.req.cookies.cookiename,
                        model_id: MODEL_ID,
                        counter: {
                            gt: log[0].max_attempts
                        }
                    }
                }, function(error, logincounter) {
                    if (logincounter.length > 0) {
                        var current_date = new Date();
                        var lastattempt = new Date(logincounter[0].updated);
                        var timeDiff = current_date.getTime() - lastattempt.getTime();
                        var diffsecs = Math.ceil(timeDiff / 1000);
                        if (Interval > diffsecs || diffsecs < Block_time) {
                            Statuscode.Status[421].try_after = (Block_time / 60) + " mins";
                            ctx.res.json(Statuscode.Status[421]);
                            delete Statuscode.Status[411].try_after;
                        } else {
                            callback();
                        }

                    } else {
                        callback();
                    }
                })
            } else {
                callback();
            }
        });
    };
    var check_content_type = function(content_type, callback) {
        xmlindex = content_type.split("/");
        xmlindex = xmlindex[1].indexOf("xml");
        callback(xmlindex);
    }

};
