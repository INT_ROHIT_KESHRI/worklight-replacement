angular
  .module('app')
  .factory('AuthService', ['Client', '$q', '$rootScope',function(User, $q,
      $rootScope) {
    //$rootScope.currentUser={};
    function login(email, password) {
      return User
        .login({email: "myRealm:"+email, password: password})
        .$promise
        .then(function(response) {
            $rootScope.currentUser = {
              id: response.user.id,
              tokenId: response.id,
              email: response.user.email,
              timeout:response.user.timeout,
              realm:"myRealm"
            };
        },function(error){
          return false;
        });
    }

    function adminlogin(email, password) {
      return User
        .login({email: "Superadmin:"+email, password: password})
        .$promise
        .then(function(response) {
             $rootScope.currentUser = {
               id: response.user.id,
               tokenId: response.id,
               email: response.user.email,
               timeout:response.user.timeout,
               realm:"Superadmin"
             }
        },function(error){
          return false;
        });
    }
    function resetpassword(email) {
      return User.resetPassword({email: email})
        .$promise
        .then(function(value,responseheader) {
          return true;
      },function(err){
          return false;
        });
    }

    function logout() {
      return User
       .logout()
       .$promise
       .then(function() {
         $rootScope.currentUser = null;
       });
    }

    function register(name,username,email,description,password) {
      return User
        .create({
         email: email,
         username: username,
         email: email,
         description: description,
         password: password
       })
       .$promise;
    }

    return {
      login: login,
      logout: logout,
      register: register,
      adminlogin:adminlogin,
      resetpassword:resetpassword
    };
  }]);
