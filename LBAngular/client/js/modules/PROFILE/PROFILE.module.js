angular
.module('admin.profile', [
  'ui.router',
  'lbServices',
  'ngCookies',
  'ui.bootstrap',

  'SmartAdmin.Layout',
  'SmartAdmin.UI'
])
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
  //$urlRouterProvider.otherwise('/change-password');
  $stateProvider

  .state('app.change-password', {
      url: '/change-password',
      views: {
          "content@app": {
              templateUrl: 'views/PROFILE/change-password.html',
              controller: 'ChangePasswordController'
          }
      },
      data:{
          title: 'CHANGE PASSWORD'
      }
  })
  .state('app.edit-profile', {
      url: '/edit-profile',
      views: {
          "content@app": {
              templateUrl: 'views/PROFILE/edit-profile.html',
              controller: 'EditProfileController'
          }
      },
      data:{
          title: 'EDIT PROFILE'
      }
  })
  .state('app.edit-user', {
      url: '/edit-user/:id',
      views: {
          "content@app": {
              templateUrl: 'views/PROFILE/create-profile.html',
              controller: 'EditUserController'
          }
      },
      data:{
          title: 'EDIT USER'
      }
  })
  .state('app.delete-user', {
      url: '/delete-user/:id',
      views: {
          "content@app": {
              controller: 'DeleteUserController'
          }
      }
  })
  .state('app.create-profile', {
      url: '/create-profile',
      views: {
          "content@app": {
              templateUrl: 'views/PROFILE/create-profile.html',
              controller: 'CreateProfileController'
          }
      },
      data:{
          title: 'CREATE PROFILE'
      }
  })
  .state('app.all-users', {
      url: '/all-users',
      views: {
          "content@app": {
              templateUrl: 'views/PROFILE/all-users.html',
              controller: 'AllUsersController'
          }
      },
      data:{
          title: 'ALL USERS'
      }
  })
  /*.state('app.change-password', {
      url: '/add-model',
      views: {
          "content@app": {
              templateUrl: 'views/MODEL/model-form.html',
              controller: 'AddModelController'
          }
      },
      data:{
          title: 'Create APP'
      }
  })
  .state('app.delete-model', {
      url: '/delete-model/:id',
      views: {
          "content@app": {
              controller: 'DeleteModelController'
          }
      }
  })
  .state('app.edit-model', {
      url: '/edit-model/:id',
      views: {
          "content@app": {
              templateUrl: 'views/MODEL/model-form.html',
              controller: 'EditModelController'
          }
      },
      data:{
          title: 'Edit APP'
      }
    })*/
}])
