angular
.module('admin.profile')
.controller('ChangePasswordController', ['$scope', 'Client','$cookieStore',
    '$state', function($scope,  Client, $cookieStore,
    $state) {
    var currentUser=$cookieStore.get('cookiesvalue');
    if(currentUser){
    $scope.action = 'Change';
    Client.getCurrent(function(value,header){
      $scope.user=value;
    });
    $scope.submitForm = function() {
        if($scope.user.new_password!=$scope.user.confirm_password){
          $scope.passerror="New password and confirmation password doesnot match.";
          $scope.passerroricon="fa fa-warning text-danger";
          return false;
        }
          Client.prototype$updateAttributes({id:$scope.user.id},{"password": $scope.user.new_password}, function (value, headers) {
                $scope.passsuccess="Password updated successfully.";
                $scope.passsuccessicon="fa fa-thumbs-o-up text-success";
                $scope.passerror="";
                $scope.passerroricon="";
                return false;
              },function(err){
              });
      };
    }else{
       $state.go('login');
    }

}])
.controller('EditProfileController', ['$scope', 'Client', '$cookieStore',
    '$state', function($scope,  Client, $cookieStore,
     $state) {
    var currentUser=$cookieStore.get('cookiesvalue');
    if(currentUser){
    $scope.action = 'Save';
    Client.getCurrent(function(value,headers){
      $scope.client=value;
    }, function(err){
      alert("Something went wrong.Please refresh the page");
    });
    $scope.submitForm = function() {
      Client.prototype$updateAttributes({id:currentUser.id}, {description:$scope.client.description,name:$scope.client.name}, function(value,headers){
          alert("Successfully updated");
        }, function(err){
          alert("Unable to Update.Please try again latter");
        });
    }
    }else{
       $state.go('login');
    }

}])
.controller('CreateProfileController', ['$scope', 'Client','$cookieStore',
    '$state','Mail',function($scope,  Client,$cookieStore,
    $state,Mail) {
    var currentUser=$cookieStore.get('cookiesvalue');
    if(currentUser.realm=='Superadmin'){
      Client.getCurrent(function(value,header){
        if(value.realm=='Superadmin'){
          $scope.action = 'Create';
          $scope.submitForm = function() {
            if($scope.client.status==1){
              $scope.realm="myRealm";
            }else{
              $scope.realm="";
            }
            if( $scope.client.timeout==undefined ||  $scope.client.timeout < 0){
              $scope.timerror="Please put some valid time to proceed.";
              $scope.timerroricon="fa fa-warning text-danger";
              return false;
            }
            if( $scope.client.apps==undefined ||  $scope.client.apps < 0){
              $scope.apperror="Please put some valid number to proceed.";
              $scope.apperroricon="fa fa-warning text-danger";
              return false;
            }
            if( $scope.client.validity==undefined ||  $scope.client.validity < 0){
              $scope.apperror="Please put some valid number to proceed.";
              $scope.apperroricon="fa fa-warning text-danger";
              return false;
            }else{
              var cur = new Date();
              var validity = cur.setDate(cur.getDate() + $scope.client.validity);
            }
            var ascess_level={
              cache:$scope.client.cache,
              ratelimiter:$scope.client.ratelimiter,
              responsetime:$scope.client.responsetime
            };
            //$scope.client.password = Math.floor((Math.random() * 1000000) + 1);
            Client.findOne({filter:{where:{email:$scope.client.email}}},function(value,headers){
              $scope.emailerror="Email already exists.Please try with diffrent email address.";
              $scope.emailerroricon="fa fa-warning text-danger";
              return false;
            }, function(err){
              //alert(password);
              Client
                .create({
                 realm: $scope.realm,
                 name: $scope.client.name,
                 apps:$scope.client.apps,
                 email: $scope.client.email,
                 description: $scope.client.description,
                 password: $scope.client.password,
                 timeout: $scope.client.timeout,
                 status:$scope.client.status,
                 ascess_level:JSON.stringify(ascess_level),
                 validity_to:validity,
                 validity_from:new Date(),
                 created:new Date(),
                 modified:new Date()
               },function(value,headers){
                 Mail.sendEmail({
                   id:value.id,
                   type:'welcome',
                   optional:$scope.client.password
                 },function(data){
                 });
                 $state.go('app.all-users');
               }, function(err){
                 alert("Something went wrong.Please refresh the page");
               });
            });
          }
        }else{
           $state.go('login');
        }
      });

    }else{
       $state.go('login');
    }

}])
.controller('AllUsersController', ['$scope', 'Client','Model','Mail','$cookieStore','$window',
    '$state', '$uibModal', '$log','$filter', function($scope, Client , Model ,Mail, $cookieStore,$window,
    $state, $uibModal, $log,$filter) {
    var currentUser=$cookieStore.get('cookiesvalue');
    if(currentUser.realm=='Superadmin'){
      Client.getCurrent(function(value,header){
        if(value.realm=='Superadmin'){
          $scope.client_id=currentUser.id;
          $scope.datas = [];
          $scope.tempDatasArr = [];

          $scope.maxSize = 5;
          $scope.bigTotalItems;
          $scope.itemsPerPage = 8;
          $scope.bigCurrentPage = 1;
          $scope.begin = 0;

          $scope.search = function(val) {
            $scope.bigCurrentPage = 1;
            $scope.begin = 0;
            $scope.datas = $filter('filter')($scope.tempDatasArr,val);
          }

          $scope.$watch('datas',function(newVal) {
            $scope.bigTotalItems = newVal.length;
          })
          var init = function() {
            Client.find({filter:{where:{id:{neq:currentUser.id}}}},function(response,headers){
             $scope.users = response.reverse();
             $scope.tempDatasArr = $scope.users;
             $scope.datas = $scope.tempDatasArr;
             $scope.bigTotalItems = $scope.datas.length;
            });
          }
          init();
          $scope.Status_Update=function(flag,id){

            $scope.message = "By changing the status of a user. All the activity of the user changed accordingly";
            var modalInstance = $uibModal.open({
                animation: true,
                scope : $scope,
                templateUrl: 'views/modal.html',
                controller: 'ModalInstanceCtrl'
            });

            modalInstance.result.then(function (status) {
                var realm;
                 if(status) {
                   if(flag==1){
                     realm='';
                     flag=0;
                   }else{
                     realm='myRealm';
                     flag=1;
                   }
                   Model.find({filter:{where:{client_id:id}}},function(response,header){
                     response.forEach(function(fieldname, index) {
                       Model.prototype$updateAttributes({id:fieldname.id},{statuschange:1,flag:flag});
                     });
                     Client.updateAll({where:{id:id}},{status:flag,realm:realm},function(client,clientheader){
                       if(flag==0){
                         Mail.sendEmail({
                           id:id,
                           type:'disable',
                           optional:'blank'
                         },function(data){
                         });
                       }
                       $window.location.reload();
                     },function(err){
                       alert("Something went wrong.Please try again latter.");
                     });
                   });
                 }else {
                     return false;
                 }
             });
          };

          $scope.Delete_User=function(id){
            $scope.message = "Are you absolutely sure you want to delete?";
            var modalInstance = $uibModal.open({
                animation: true,
                scope : $scope,
                templateUrl: 'views/modal.html',
                controller: 'ModalInstanceCtrl'
            });
            modalInstance.result.then(function (status) {
                 if(status) {
                   Model.find({filter:{where:{client_id:id}}},function(response){
                     response.forEach(function(fieldname, index) {
                       Model.deleteById({id:fieldname.id});
                     });
                     Mail.sendEmail({
                       id:id,
                       type:'delete',
                       optional:'blank'
                     },function(data){
                       Client
                      .deleteById({ id:id })
                      .$promise
                      .then(function() {
                         $window.location.reload();
                      });
                     });
                   })
                 }else {
                   return false;
                 }
             },function(){
               return false;
             });
          };

          $scope.reminder_mail=function(id){
            Mail.sendEmail({
              id:id,
              type:'reminder',
              optional:'blank'
            },function(data){
            });
          };
          $scope.Days_remaining=function(to){
            var validity_to = new Date(to);
            var cur_date = new Date();
            var timeDiff = (validity_to.getTime() - cur_date.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
            return diffDays>0?diffDays:'Late by '+Math.abs(diffDays);
          };
        }else{
          $state.go('login');
        }
      });

    }else{
       $state.go('login');
    }

}])
.controller('EditUserController', ['$scope', 'Client','$cookieStore',
    '$state','$stateParams','Mail',function($scope,  Client, $cookieStore ,
    $state,$stateParams ,Mail) {
    var currentUser=$cookieStore.get('cookiesvalue');
    if(currentUser.realm=='Superadmin'){
      Client.getCurrent(function(value,header){
        if(value.realm=='Superadmin'){
          $scope.action = 'Edit';
          $scope.passDisable=true;
          $scope.emailDisable=true;
          $scope.master={};
          $scope.previous_field_values={};
          Client.find({filter:{where:{id:$stateParams.id}}},function(value,headers){
            $scope.client=value[0];
            var validity_to = new Date(value[0].validity_to);
            var validity_from = new Date(value[0].validity_from);
            var timeDiff = Math.abs(validity_to.getTime() - validity_from.getTime());
            $scope.client.validity=diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
            $scope.client.timeout=parseInt(value[0].timeout);
            $scope.client.apps=parseInt(value[0].apps);
            var ascess_level=JSON.parse(value[0].ascess_level);
            $scope.client.cache=ascess_level.cache;
            $scope.client.ratelimiter=ascess_level.ratelimiter;
            $scope.client.responsetime=ascess_level.responsetime;
            /*Previous form field values */
              $scope.previous_field_values.previous_ascess_level=JSON.parse(value[0].ascess_level);
              var previous_validity_to = new Date(value[0].validity_to);
              var previous_validity_from = new Date(value[0].validity_from);
            //  var previous_timeDiff = Math.abs(previous_validity_to.getTime() - previous_validity_from.getTime());
              $scope.previous_field_values.validity=diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
              $scope.previous_field_values.timeout=parseInt(value[0].timeout);
              $scope.previous_field_values.apps=parseInt(value[0].apps);
              $scope.previous_field_values.cache=$scope.previous_field_values.previous_ascess_level.cache;
              $scope.previous_field_values.ratelimiter=$scope.previous_field_values.previous_ascess_level.ratelimiter;
              $scope.previous_field_values.responsetime=$scope.previous_field_values.previous_ascess_level.responsetime;
              angular.copy($scope.previous_field_values, $scope.master);
            /*End*/
          });
          $scope.submitForm = function(form) {
            if($scope.client.status==1){
              $scope.client.realm="myRealm";
            }else{
              $scope.client.realm="";
            }
            if($scope.client.timeout==undefined ||  $scope.client.timeout < 0){
              $scope.timerror="Please put some valid time to proceed.";
              $scope.timerroricon="fa fa-warning text-danger";
              return false;
            }
            if( $scope.client.apps==undefined ||  $scope.client.apps < 0){
              $scope.apperror="Please put some valid number to proceed.";
              $scope.apperroricon="fa fa-warning text-danger";
              return false;
            }
            if( $scope.client.validity==undefined ||  $scope.client.validity < 0){
              $scope.apperror="Please put some valid number to proceed.";
              $scope.apperroricon="fa fa-warning text-danger";
              return false;
            }else{
              var cur = new Date($scope.client.validity_from);
              var validity = cur.setDate(cur.getDate() + $scope.client.validity);
              $scope.client.validity_to=validity;
            }
            var ascess_level={
              cache:$scope.client.cache,
              ratelimiter:$scope.client.ratelimiter,
              responsetime:$scope.client.responsetime
            };
            $scope.client.ascess_level=JSON.stringify(ascess_level);
            Client.updateAll({where:{id:$stateParams.id}},$scope.client,function(client,clientheader){
              if(form.$dirty==true){
                if($scope.client.status==true){
                  var form_changes={};
                  if(form.apps.$dirty==true){
                    form_changes.apps=($scope.client.apps > $scope.master.apps)?"Increased by "+($scope.client.apps-$scope.master.apps):"Decreased by "+($scope.master.apps-$scope.client.apps);
                  }
                  if(form.validity.$dirty==true){
                    form_changes.validity=($scope.client.validity > $scope.master.validity)?"Extended by "+($scope.client.validity-$scope.master.validity):"Degraded by "+($scope.master.validity-$scope.client.validity);
                  }
                  Mail.sendEmail({
                    id:$stateParams.id,
                    type:'update',
                    optional:form_changes
                  },function(data){
                  });
                }else{
                  Mail.sendEmail({
                    id:$stateParams.id,
                    type:'disable',
                    optional:'blank'
                  },function(data){
                  });
                }
              }
              $state.go('app.all-users');
            },function(err){
              alert("Something went wrong.Please try again latter.");
            });
          }
        }else{
          $state.go('login');
        }
      });
    }else{
       $state.go('login');
    }

}])
