angular
.module('admin.restclient', [
  'ui.router',
  'lbServices',
  'ngCookies',
  'ui.bootstrap',

  'SmartAdmin.Layout',
  'SmartAdmin.UI'
])
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/all-caches');
  $stateProvider
  .state('app.rest-client', {
      url: '/rest-client',
      views: {
          "content@app": {
              templateUrl: 'views/RESTCLIENT/restclient-form.html',
              controller: 'GetResponseController'
          }
      },
      data:{
          title: 'Test API'
      }
  })
}])
