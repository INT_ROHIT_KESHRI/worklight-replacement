angular
.module('admin.restclient')
.controller('GetResponseController', ['$scope','$cookieStore',
    '$state','$http', function($scope, $cookieStore, $state,$http) {
      var currentUser=$cookieStore.get('cookiesvalue');
      if(currentUser){
        $scope.headerchoices = [{header_param:'param1',value:'value1'}];
        $scope.addHeaderChoice = function() {
        var newItemNo = $scope.headerchoices.length+1;
          $scope.headerchoices.push({'header_param':'param'+newItemNo,'value':'value'+newItemNo});
          $scope.headerchoices[newItemNo-1].id;
        };
        $scope.removeHeaderChoice = function() {
          var lastItem = $scope.headerchoices.length-1;
          $scope.headerchoices.splice(lastItem);
        };

        $scope.bodychoices = [{body_param:'param1',value:'value1'}];
        $scope.addBodyChoice = function() {
          if($scope.restclient.method=='GET'){
            return false;
          }
          var newItemNo = $scope.bodychoices.length+1;
          $scope.bodychoices.push({'body_param':'param'+newItemNo,'value':'value'+newItemNo});
        };
        $scope.removeBodyChoice = function() {
          var lastItem = $scope.bodychoices.length-1;
          $scope.bodychoices.splice(lastItem);
        };

        $scope.obj = {
          options: {
            mode: 'code',
            modes: ['code', 'form', 'text', 'tree', 'view']
          }
        };
        $scope.onLoad = function (instance) {
            instance.expandAll();
        };
        $scope.tabs = [{
            title: 'Header',
            url: 'one.tpl.html'
        }, {
            title: 'Body',
            url: 'two.tpl.html'
        }];

        $scope.currentTab = 'one.tpl.html';
        $scope.onClickTab = function (tab) {
            $scope.currentTab = tab.url;
        }
        $scope.isActiveTab = function(tabUrl) {
            return tabUrl == $scope.currentTab;
        }

        $scope.update_method=function(){
          if($scope.restclient.method=='GET'){
            $scope.bodychoices=[];
          }
        }
        $scope.submitForm = function() {

          var restclient={};
          restclient['header']={};
          restclient['body']={};
          restclient['others']={};
          restclient.others.url=$scope.restclient.url;
          restclient.others.method=$scope.restclient.method;
          angular.forEach($scope.headerchoices, function(value, key) {
            if(value.header_param!=''){
              restclient.header[value.header_param]=value.value;
            }
          });
          restclient.header['Content-Type']='application/x-www-form-urlencoded';
          var str='';
          angular.forEach($scope.bodychoices, function(value, key) {
            if(value.body_param!=''){
              str=value.body_param+'='+value.value+'&'+str;
            }
          });
          var newStr = str.substring(0, str.length-1);
          $scope.restclient.response={};

          $http({
              method :restclient.others.method,
              url : restclient.others.url,
              data:newStr,
              withCredentials: true,
              headers:restclient.header
              }).then(function mySucces(response) {
                  $scope.restclient.response.body=response.data;
                  $scope.restclient.response.headers=response.config.headers;
                  $scope.restclient.response.status="Status : "+response.status+" "+response.statusText;
              }, function myError(response) {
                  $scope.myWelcome = response.statusText;
          });
        };

      }else{
        $state.go('login')
      }

    }])
