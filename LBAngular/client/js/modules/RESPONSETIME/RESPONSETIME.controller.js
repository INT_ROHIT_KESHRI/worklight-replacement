angular
.module('admin.responsetime')
.controller('AllResponsetimesController', ['$scope', 'Api','Responsetime','$cookieStore', '$filter', '$state','$window','$uibModal', function($scope,Api,Responsetime,$cookieStore, $filter, $state,$window,$uibModal) {
  var currentUser=$cookieStore.get('cookiesvalue');
  if(currentUser) {
    $scope.client_id=currentUser.id;
    $scope.datas = [];
    $scope.tempDatasArr = [];
    var api_id=[];
    var api_details=[];

    $scope.maxSize = 5;
    $scope.bigTotalItems;
    $scope.itemsPerPage = 8;
    $scope.bigCurrentPage = 1;
    $scope.begin = 0;

    $scope.search = function(val) {
      $scope.bigCurrentPage = 1;
      $scope.begin = 0;
      $scope.datas = $filter('filter')($scope.tempDatasArr,val);
    }

    $scope.$watch('datas',function(newVal) {
      $scope.bigTotalItems = newVal.length;
    })
    var init = function() {
      Responsetime.find({filter:{where:{client_id:$scope.client_id},include:['api']}}).$promise.then(function(response) {
          $scope.responsetimes = response.reverse();
          $scope.tempDatasArr = $scope.responsetimes;
          $scope.datas = $scope.tempDatasArr;
          $scope.bigTotalItems = $scope.datas.length;
      });
    }
    init();
    $scope.Status_Update=function(flag,id){
      flag=(flag==1)?0:1;
      Responsetime.prototype$updateAttributes({id:id},{flag:flag},function(success){
        $window.location.reload();
      },function(err){
        alert("Unable to update.Please try again latter");
        return false;
      });
    };
    $scope.Delete_Responsetime=function(id){
      $scope.message = "Are you absolutely sure you want to delete?";
      var modalInstance = $uibModal.open({
          animation: true,
          scope : $scope,
          templateUrl: 'views/modal.html',
          controller: 'ModalInstanceCtrl'
      });
      modalInstance.result.then(function (status) {
           if(status) {
             Responsetime
               .deleteById({ id:id })
               .$promise
               .then(function() {
                 $window.location.reload();
               });
           }else {
             return false;
           }
       },function(){
         return false;
       });
    };
  }else {
    $state.go('login');
  }

}])
.controller('AddResponsetimeController', ['$scope', 'Responsetime', 'Auth', 'Api','Client','$cookieStore',
    '$state', function($scope, Responsetime, Auth, Api , Client, $cookieStore, $state) {
      var currentUser=$cookieStore.get('cookiesvalue');
      var api_id=[];
      $scope.client_id=currentUser.id;
      $scope.action = 'Add';
      $scope.apis = [];
      $scope.selectedapi;
      $scope.isDisabled = false;
      $scope.responsetime={};
      $scope.responsetime_ascess=false;
      if(currentUser){
        Client.getCurrent(function(value,headers){
          var ascess_level=JSON.parse(value.ascess_level);
          if(ascess_level.responsetime && ascess_level.responsetime==true){
            $scope.responsetime_ascess=true;
          }
        });
      Responsetime
        .find()
        .$promise
        .then(function(responsetimes) {
          angular.forEach(responsetimes, function (responsetime, responsetimeIndex) {
            api_id[responsetimeIndex]=responsetime.api_id;
          });
          var A = function(callback) {
            Auth.find({filter:{where:{client_id:$scope.client_id}}},function(authresponse,authheader){
                if(authresponse[0]){
                  angular.forEach(authresponse, function(value, key) {
                    api_id.push(value.api_id);
                  });
                }
              });
              callback();
          }
          var B=function(){
            setTimeout(function(){
            Api
              .find({filter:{where:{is_logout:{neq:1},client_id:$scope.client_id,id:{nin:api_id},flag:1}}})
              .$promise
              .then(function(apis) {
              $scope.apis = apis;
              $scope.selectedapi = $scope.selectedapi || apis[0];
            });
          },500);
          }
        A(function() {
          B();
        });
        });
       $scope.submitForm = function() {
         if($scope.responsetime_ascess==false){
           $scope.apierror="You are not eligible to access the response time features. Contact the administrator.";
           $scope.apierroricon="fa fa-warning text-danger";
           return false;
         }
         if($scope.selectedapi==undefined){
           $scope.apierror="Please add a api to add responsetime";
           $scope.apierroricon="fa fa-warning text-danger";
           return false;
         }
         if( $scope.responsetime.time==undefined ||  $scope.responsetime.time < 0){
           $scope.responsetimerror="Please put some time to proceed.";
           $scope.responsetimerroricon="fa fa-warning text-danger";
           return false;
         }
          Responsetime
            .create({
              api_id: $scope.selectedapi.id,
              client_id:$scope.client_id,
              time: $scope.responsetime.time
            })
            .$promise
            .then(function() {
              $state.go('app.all-responsetimes');
            });
        };
     }else{
       $state.go('login');
     }

    }])
