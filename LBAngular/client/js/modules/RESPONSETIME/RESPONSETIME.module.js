angular
.module('admin.responsetime', [
  'ui.router',
  'lbServices',
  'ngCookies',
  'ui.bootstrap',

  'SmartAdmin.Layout',
  'SmartAdmin.UI'
])
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/all-caches');
  $stateProvider
  .state('app.all-responsetimes', {
      url: '/all-responsetimes',
      views: {
          "content@app": {
              templateUrl: 'views/RESPONSETIME/all-responsetimes.html',
              controller: 'AllResponsetimesController'
          }
      },
      data:{
          title: 'All RESPONSETIME'
      }
  })
  .state('app.add-responsetime', {
      url: '/add-responsetime',
      views: {
          "content@app": {
              templateUrl: 'views/RESPONSETIME/responsetime-form.html',
              controller: 'AddResponsetimeController'
          }
      },
      data:{
          title: 'Add RESPONSETIME'
      }
  })
  .state('app.edit-responsetime', {
      url: '/edit-responsetime/:id',
      views: {
          "content@app": {
              templateUrl: 'views/RESPONSETIME/responsetime-form.html',
              controller: 'EditResponsetimeController'
          }
      },
      data:{
          title: 'Edit RESPONSETIME'
      }
  })
  .state('app.delete-responsetime', {
      url: '/delete-responsetime/:id',
      views: {
          "content@app": {
              controller: 'DeleteResponsetimeController'
          }
      }
  })
}])
