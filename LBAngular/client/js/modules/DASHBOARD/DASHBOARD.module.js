angular
.module('admin.dashboard', [
  'ui.router',
  'lbServices',
  'ngCookies',
  'ui.bootstrap',
  'SmartAdmin.Layout',
  'SmartAdmin.UI'
])
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/auth-list');
  $stateProvider
  .state('app.dashboard', {
      url: '/dashboard',
      views: {
          "content@app": {
              templateUrl: 'views/DASHBOARD/dashboard.html',
              controller: 'DashboardController'
          }
      },
      data:{
          title: 'Analytics Data'
      }
  })
}])
