angular
.module('admin.model')
.controller('AllModelsController', ['$scope', 'Model','$cookieStore','$filter', '$state','$window','$uibModal', function($scope,Model,$cookieStore,$filter, $state,$window,$uibModal) {
  var currentUser=$cookieStore.get('cookiesvalue');
  if(currentUser) {
    $scope.client_id=currentUser.id;
    $scope.datas = [];
    $scope.tempDatasArr = [];

    $scope.maxSize = 5;
    $scope.bigTotalItems;
    $scope.itemsPerPage = 8;
    $scope.bigCurrentPage = 1;
    $scope.begin = 0;

    $scope.search = function(val) {
      $scope.bigCurrentPage = 1;
      $scope.begin = 0;
      $scope.datas = $filter('filter')($scope.tempDatasArr,val);
    }

    $scope.$watch('datas',function(newVal) {
      $scope.bigTotalItems = newVal.length;
    })
    var init = function() {
      Model.find({filter:{where:{client_id:$scope.client_id}}},function(response,responseHeaders){
       $scope.models = response.reverse();
       $scope.tempDatasArr = $scope.models;
       $scope.datas = $scope.tempDatasArr;
       $scope.bigTotalItems = $scope.datas.length;
      });
    }

    init();
    $scope.Status_Update=function(flag,id){
      $scope.message = "By changing the status.All the linked api status changed accordingly";
      var modalInstance = $uibModal.open({
          animation: true,
          scope : $scope,
          templateUrl: 'views/modal.html',
          controller: 'ModalInstanceCtrl'
      });

      modalInstance.result.then(function (status) {
           if(status) {
             flag=(flag==1)?0:1;
             Model.prototype$updateAttributes({id:id},{statuschange:1,flag:flag},function(success){
               $window.location.reload();
             },function(err){
               alert("Unable to update.Please try again latter");
               return false;
             });
           }else {
               return false;
           }
       });
    };
    $scope.Delete_App=function(id){
      $scope.message = "Are you absolutely sure you want to delete?";
      var modalInstance = $uibModal.open({
          animation: true,
          scope : $scope,
          templateUrl: 'views/modal.html',
          controller: 'ModalInstanceCtrl'
      });
      modalInstance.result.then(function (status) {
           if(status) {
             Model
              .deleteById({ id:id })
              .$promise
              .then(function() {
                $window.location.reload();
              });
           }else {
            return false;
           }
       },function(){
         return false;
       });
    };

  }else {
    $state.go('login');
  }

}])
.controller('AddModelController', ['$scope', 'Model', 'Auth','Client','$cookieStore',
    '$state', function($scope, Model, Auth, Client, $cookieStore, $state) {
  var currentUser=$cookieStore.get('cookiesvalue');
  $scope.client_id=currentUser.id;
  $scope.action = 'Add';
  $scope.isDisabled = false;
  if(currentUser) {
    Client.getCurrent(function(value,headers){
      Model.count({where:{client_id:currentUser.id}},function(response,responseHeaders){
      $scope.createdappsnumber=response.count;
      $scope.appsnumber=value.apps;
      });
    });

  $scope.submitForm = function() {
    if($scope.createdappsnumber==$scope.appsnumber){
      $scope.error="You have crossed the level to create App.Contact the administrator";
      $scope.erroricon="fa fa-warning text-danger";
      return false;
    }else{
      Model.count({where:{name:$scope.model.name}},function(response,responseHeaders){
        if(response.count==1){
          $scope.error="Auth already exist";
          $scope.erroricon="fa fa-warning text-danger";
          return false;
        }else{
          Model
            .create({
              name: $scope.model.name,
              client_id: $scope.client_id,
              group_name: $scope.model.group_name,
              description: $scope.model.description
            })
            .$promise
            .then(function(data) {
              if (data.Response) {
                $scope.error=data.Response;
              }else{
                $state.go('app.all-models');
              }
            });
        }
      });
    }
  };
  }else{
  $state.go('login');
  }
}])

.controller('EditModelController', ['$scope', 'Api', 'Model','$q','$cookieStore',
    '$stateParams', '$state', function($scope,  Api, Model,$q,$cookieStore,
    $stateParams, $state) {
    var currentUser=$cookieStore.get('cookiesvalue');
    $scope.action = 'Save';
    $scope.model = {};
    $scope.isDisabled = false;
      Model.findById({ id: $stateParams.id }).$promise
    .then(function(model) {
      $scope.model = model;
    });
    if(currentUser){
      $scope.submitForm = function() {
        Model.count({where:{name:$scope.model.name,id:{neq:$stateParams.id}}},function(response,responseHeaders){
          if(response.count==1){
            $scope.error="Auth already exist";
            $scope.erroricon="fa fa-warning text-danger";
            return false;
          }else{
            $scope.model
              .$save()
              .then(function(model) {
                $state.go('app.all-models');
              });
          }
        });
      };
    }else{
       $state.go('login');
    }

}])
