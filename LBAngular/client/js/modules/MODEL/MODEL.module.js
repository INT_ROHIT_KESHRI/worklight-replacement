angular
.module('admin.model', [
  'ui.router',
  'lbServices',
  'ngCookies',
  'ui.bootstrap',

  'SmartAdmin.Layout',
  'SmartAdmin.UI'
])
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/all-models');
  $stateProvider

  .state('app.all-models', {
      url: '/all-models',
      views: {
          "content@app": {
              templateUrl: 'views/MODEL/all-models.html',
              controller: 'AllModelsController'
          }
      },
      data:{
          title: 'All APPS'
      }
  })
  .state('app.add-model', {
      url: '/add-model',
      views: {
          "content@app": {
              templateUrl: 'views/MODEL/model-form.html',
              controller: 'AddModelController'
          }
      },
      data:{
          title: 'Create APP'
      }
  })
  .state('app.delete-model', {
      url: '/delete-model/:id',
      views: {
          "content@app": {
              controller: 'DeleteModelController'
          }
      }
  })
  .state('app.edit-model', {
      url: '/edit-model/:id',
      views: {
          "content@app": {
              templateUrl: 'views/MODEL/model-form.html',
              controller: 'EditModelController'
          }
      },
      data:{
          title: 'Edit APP'
      }
  })
}])
