angular
.module('admin.api')
.controller('AllApisController', ['$scope','Auth','Api','$cookieStore', '$filter', '$state','$window','$uibModal', function($scope,Auth,Api,$cookieStore,$filter,$state,$window,$uibModal) {
  var currentUser=$cookieStore.get('cookiesvalue');
  if(currentUser) {
    $scope.client_id=currentUser.id;
    $scope.datas = [];
    $scope.tempDatasArr = [];
    var authapis=[];
    $scope.maxSize = 5;
    $scope.bigTotalItems;
    $scope.itemsPerPage = 8;
    $scope.bigCurrentPage = 1;
    $scope.begin = 0;

    $scope.search = function(val) {
      $scope.bigCurrentPage = 1;
      $scope.begin = 0;
      $scope.datas = $filter('filter')($scope.tempDatasArr,val);
    }

    $scope.$watch('datas',function(newVal) {
      $scope.bigTotalItems = newVal.length;
    });
    var init = function() {
      var A = function(callback) {
          Auth.find({filter:{where:{client_id:$scope.client_id}}},function(authresponse,authheader){

              if(authresponse[0]){
                angular.forEach(authresponse, function(value, key) {
                  authapis[key]=value.api_id;
                });
              }
            });
          callback();
      };
      var B = function() {
          setTimeout(function(){
            Api.find({filter:{where:{is_logout:{neq:1},client_id:$scope.client_id,id:{nin:authapis}}}},function(response,responseHeaders){
            //  alert('authresponse');
             $scope.apis = response.reverse();
             $scope.tempDatasArr = $scope.apis;
             $scope.datas = $scope.tempDatasArr;
             $scope.bigTotalItems = $scope.datas.length;
            });
          },600);
      };
        A(function() {
          B();
        });
    }
    init();
    $scope.Status_Update=function(flag,id){
      flag=(flag==1)?0:1;
      Api.prototype$updateAttributes({id:id},{statuschange:1,flag:flag},function(success){
        $window.location.reload();
      },function(err){
        alert("Unable to update.Please try again latter");
        return false;
      });
    };

    $scope.Delete_Api=function(id){
      $scope.message = "Are you absolutely sure you want to delete?";
      var modalInstance = $uibModal.open({
          animation: true,
          scope : $scope,
          templateUrl: 'views/modal.html',
          controller: 'ModalInstanceCtrl'
      });
      modalInstance.result.then(function (status) {
           if(status) {
             Api
             .deleteById({ id:id })
             .$promise
             .then(function() {
               $window.location.reload();
             });
           }else {
             return false;
           }
       },function(){
         return false;
       });
    };
  }else {
    $state.go('login');
  }

}])
.controller('AddApiController', ['$scope', 'Model', 'Api','Auth','$cookieStore',
    '$state', function($scope, Model, Api, Auth, $cookieStore, $state) {
  var currentUser=$cookieStore.get('cookiesvalue');
  $scope.client_id=currentUser.id;
  $scope.action = 'Add';
  $scope.models = [];
  $scope.selectedmodel;
  $scope.isDisabled = false;
  if(currentUser) {
  var json = {};
  $scope.obj = {
    data: json,
    options: {
      mode: 'code'
    }
    };
  $scope.onLoad = function (instance) {
      instance.expandAll();
  };

  $scope.modelchange=function(){
    Auth.count({where:{model_id:$scope.selectedmodel.id}},function(response,responseHeaders){
      if(response.count==1){
        $scope.disabletoken=false;
        $scope.api.token=true;
      }else{
        $scope.disabletoken=true;
        $scope.api.token=false;
      }
    });
  }
  Model
    .find({filter:{where:{client_id:$scope.client_id,flag:1}}})
    .$promise
    .then(function(models) {
    $scope.models = models;
    $scope.selectedmodel = $scope.selectedmodel;
    });

      $scope.submitForm = function() {
        if($scope.selectedmodel==undefined){
          $scope.modelerror="Please select a App.";
          $scope.modelerroricon="fa fa-warning text-danger";
          return false;
        }
        if($scope.api.method=='POST'){
          if($scope.api.fields==undefined){
            $scope.api.parameter="Parameter field required.";
            $scope.api.paramerroricon="fa fa-warning text-danger";
            return false;
          }
        }
        Api.count({where:{name:$scope.api.name,model_id:$scope.selectedmodel.id}},function(response,responseHeaders){
          if(response.count==1){
            $scope.error="Api name already exist.Please try diffrent name.";
            $scope.apierroricon="fa fa-warning text-danger";
            return false;
          }else if($scope.api.method==undefined){
            $scope.methoderror="Please select e method.";
            $scope.methoderroricon="fa fa-warning text-danger";
            return false;
          }else{
            var token = ($scope.api.token==true)?1:0;
            var fields=($scope.api.fields==undefined)?{}:$scope.api.fields;
            fields=JSON.stringify(fields);
            Api
              .create({
                name: $scope.api.name,
                method: $scope.api.method,
                content_type:$scope.api.content_type,
                uri: $scope.api.uri,
                token: token,
                fields: fields,
                modelname:$scope.selectedmodel.name,
                modelid:$scope.selectedmodel.id,
                client_id:$scope.selectedmodel.client_id,
              })
              .$promise
              .then(function() {
                $state.go('app.all-apis');
              });
          }
        });
      };
    }else{
      $state.go('login');
    }

}])

.controller('EditApiController', ['$scope', 'Api', 'Model','Auth','$q',
    '$stateParams', '$state','$cookieStore', function($scope,  Api, Model, Auth, $q,
    $stateParams, $state,$cookieStore) {
    var currentUser=$cookieStore.get('cookiesvalue');
    $scope.action = 'Save';
    $scope.models = [];
    $scope.selectedmodel;
    $scope.api = {};
    $scope.isDisabled = false;
    if(currentUser) {
    $scope.modelchange=function(){
      Auth.count({where:{model_id:$scope.selectedmodel.id}},function(response,responseHeaders){
        if(response.count==1){
          $scope.disabletoken=false;
          $scope.api.token=true;
        }else{
          $scope.disabletoken=true;
          $scope.api.token=false;
        }
      });
    }
    $q.all([
      Model.find().$promise,
      Api.findById({ id: $stateParams.id }).$promise
    ])
    .then(function(data) {
      var models = $scope.models = data[0];
      $scope.api = data[1];
      $scope.selectedmodel;

      var fields = JSON.parse(data[1].fields);
      var allfields = Object.keys(fields).map(function(k) {
          return fields[k]
      });
      var fieldname;
      var fieldobj={};
      allfields.forEach(function(fieldname, index) {
          fieldname = fieldname.split("!!");
          fieldobj[fieldname[0]]=fieldname[1];
      });
      $scope.api.fields=fieldobj;
      Auth.count({where:{model_id:$scope.api.model_id}},function(response){
        if(response.count==1 && $scope.api.token==0){
          $scope.api.token=false;
          $scope.disabletoken=false;
        }else if(response.count==1 && $scope.api.token==1){
          $scope.api.token=true;
          $scope.disabletoken=false;
        }else{
          $scope.api.token=false;
          $scope.disabletoken=true;
        }
      });
      var selectedmodelIndex = models
        .map(function(modeldata) {
          return modeldata.id;
        })
        .indexOf($scope.api.model_id);
      $scope.selectedmodel = models[selectedmodelIndex];
    });
    $scope.obj = {
      options: {
        mode: 'code'
      }
      };
    $scope.onLoad = function (instance) {
        instance.expandAll();
    };

    $scope.submitForm = function() {
      if($scope.api.method=='POST'){
        if(Object.keys($scope.api.fields).length<1){
          $scope.api.parameter="Parameter field required.";
          $scope.api.paramerroricon="fa fa-warning text-danger";
          return false;
        }
      }
      var token = ($scope.api.token==true)?1:0;
      $scope.api.token = token;
      $scope.api.model_id = $scope.selectedmodel.id;
      $scope.api.modelname = $scope.selectedmodel.name;
      $scope.api.fields = JSON.stringify($scope.api.fields);
      $scope.api
        .$save()
        .then(function(api) {
          $state.go('app.all-apis');
        });
    };
  }else{
      $state.go('login');
  }
}])
