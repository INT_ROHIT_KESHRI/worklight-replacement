angular
.module('admin.api', [
  'ui.router',
  'lbServices',
  'ngCookies',
  'ui.bootstrap',

  'SmartAdmin.Layout',
  'SmartAdmin.UI'
])
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/all-apis');
  $stateProvider
  .state('app.all-apis', {
      url: '/all-apis',
      views: {
          "content@app": {
              templateUrl: 'views/API/all-apis.html',
              controller: 'AllApisController'
          }
      },
      data:{
          title: 'All APIS'
      }
  })
  .state('app.add-api', {
      url: '/add-api',
      views: {
          "content@app": {
              templateUrl: 'views/API/api-form.html',
              controller: 'AddApiController'
          }
      },
      data:{
          title: 'Add API'
      }
  })
  .state('app.edit-api', {
      url: '/edit-api/:id',
      views: {
          "content@app": {
              templateUrl: 'views/API/api-form.html',
              controller: 'EditApiController'
          }
      },
      data:{
          title: 'Edit API'
      }
  })
  .state('app.delete-api', {
      url: '/delete-api/:id',
      views: {
          "content@app": {
              controller: 'DeleteApiController'
          }
      }
  })
}])
