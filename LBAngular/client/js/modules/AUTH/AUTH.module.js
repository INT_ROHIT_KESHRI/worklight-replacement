angular
.module('admin.auth', [
  'ui.router',
  'lbServices',
  'ngCookies',
  'ui.bootstrap',
  'SmartAdmin.Layout',
  'SmartAdmin.UI'
])
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/auth-list');
  $stateProvider
  .state('app.auth-list', {
      url: '/auth-list',
      views: {
          "content@app": {
              templateUrl: 'views/AUTH/auth-list.html',
              controller: 'AuthlistController'
          }
      },
      data:{
          title: 'All AUTHS'
      }
  })
  .state('app.auth-configure', {
      url: '/auth-configure',
      views: {
          "content@app": {
              templateUrl: 'views/AUTH/auth-configure.html',
              controller: 'AuthConfigureController'
          }
      },
      data:{
          title: 'Add AUTH'
      }
  })
  .state('app.edit-auth', {
      url: '/edit-auth/:id',
      views: {
          "content@app": {
              templateUrl: 'views/AUTH/auth-configure.html',
              controller: 'EditAuthController'
          }
      },
      data:{
          title: 'Edit Auth'
      }
  })
  .state('app.delete-auth', {
      url: '/delete-auth/:id',
      views: {
          "content@app": {
              controller: 'DeleteAuthController'
          }
      }
  })
}])
