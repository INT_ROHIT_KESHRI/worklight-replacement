angular
.module('admin.auth')
.controller('AuthlistController', ['$scope','Api','Model','Auth','$cookieStore', '$filter', '$state','$window','$uibModal', function($scope, Api,Model,Auth,$cookieStore, $filter, $state,$window,$uibModal) {
  var currentUser=$cookieStore.get('cookiesvalue');
  if(currentUser) {
    $scope.client_id=currentUser.id;
    $scope.datas = [];
    $scope.tempDatasArr = [];

    $scope.maxSize = 5;
    $scope.bigTotalItems;
    $scope.itemsPerPage = 8;
    $scope.bigCurrentPage = 1;
    $scope.begin = 0;

    $scope.search = function(val) {
      $scope.bigCurrentPage = 1;
      $scope.begin = 0;
      $scope.datas = $filter('filter')($scope.tempDatasArr,val);
    }

    $scope.$watch('datas',function(newVal) {
      $scope.bigTotalItems = newVal.length;
    })
    var init = function() {
      Auth.find({filter:{where:{client_id:$scope.client_id},include:['model','api']}}).$promise.then(function(response) {
        $scope.auths = response.reverse();
        $scope.tempDatasArr = $scope.auths;
        $scope.datas = $scope.tempDatasArr;
        $scope.bigTotalItems = $scope.datas.length;
      });
    }
    init();
    $scope.Delete_Auth=function(id,model_id){
      $scope.message = "Are you absolutely sure you want to delete?";
      var modalInstance = $uibModal.open({
          animation: true,
          scope : $scope,
          templateUrl: 'views/modal.html',
          controller: 'ModalInstanceCtrl'
      });
      modalInstance.result.then(function (status) {
           if(status) {
             Api.updateAll({where:{model_id:model_id}},{attribute:1,token:0},function(err,info){
               Api
                 .deleteById({id:id})
                 .$promise
                 .then(function() {
                   Api.find({filter:{where:{is_logout:1,model_id:model_id}}})
                     .$promise
                     .then(function(response){
                       Api.deleteById({ id: response[0].id });
                       $window.location.reload();
                     });
                 });
             });
           }else {
             return false;
           }
       },function(){
          return false;
       });
    };
  }else {
    $state.go('login');
  }

}])
.controller('AuthConfigureController', ['$scope','Auth' ,'Model', 'Api',
    '$state','$cookieStore','FileUploader','$timeout', function($scope, Auth, Model, Api, $state,$cookieStore,FileUploader,$timeout) {
  var currentUser=$cookieStore.get('cookiesvalue');
  if(currentUser){
    $scope.client_id=currentUser.id;
    $scope.action = 'Add';
    $scope.models = [];
    $scope.selectedmodel;
    $scope.isDisabled = false;
    $scope.disablemax_attempts=true;
    $scope.disableattempt_interval=true;
    $scope.disableblock_time=true;
    $scope.filename='';
    var model=[];
    Auth.find({filter:{fields:{model_id:true},where:{client_id:$scope.client_id}}}).$promise.then(function(model) {
        model.forEach(function(model_id, index) {
            model[index]=model_id.model_id;
        });
        Model
          .find({filter:{where:{id:{nin:model},client_id:$scope.client_id,flag:1}}})
          .$promise
          .then(function(models) {
          $scope.models = models;
          $scope.selectedmodel = $scope.selectedmodel || models[0];
          });
    });
    var json = {};
    $scope.obj = {
      data: json,
      options: {
        mode: 'code'
      }
      };

      $scope.toggle_fields=function(){
        var restrict_login = ($scope.auth.restrict_login==true)?1:0;
        if(restrict_login==1){
          $scope.disablemax_attempts=false;
          $scope.disableattempt_interval=false;
          $scope.disableblock_time=false;
        }else{
          $scope.disablemax_attempts=true;
          $scope.disableattempt_interval=true;
          $scope.disableblock_time=true;
          $scope.auth.max_attempts='';
          $scope.auth.attempt_interval='';
          $scope.auth.block_time='';
        }
      }
    /* JS File upload validation */

    /* Initialize the uploader directory */
    var uploader = $scope.uploader = new FileUploader({
      scope: $scope,                          // to automatically update the html. Default: $rootScope
      url: '/api/containers/container1/upload',
      formData: [
        { key: 't' }
      ]
    });
    /* End */

    $scope.functioname=false;
    uploader.filters.push({
        name: 'customFilter',
        fn: function (item, options) { // second user filter
          var fname=document.getElementById('fname').value;
          if(fname==''){
            $("#fileInput").val('');
            alert("Please provide the function name first.")
            return false
          }else{
            return true;
          }

        }
    });

    // REGISTER HANDLERS
    // --------------------
    uploader.onAfterAddingFile = function(item) {
      var fileInput = document.getElementById('fileInput');
      var fname=document.getElementById('fname').value;
      item.file.name=Math.floor((Math.random() * 10000000000) + 1)+"-"+fname+".js";
      if(item.file.type=='application/x-javascript' || item.file.type=='application/javascript'){
      $("#errorarea").html("");
        var file = fileInput.files[0];
          var reader = new FileReader();
          var reed= function(){
             reader.onload = function(e) {
              var content, syntax, i, err;
              try {
                content = "'use strict'; "+reader.result;
                syntax = esprima.parse(content, { tolerant: true});
                if(syntax.errors.length>0){
                  for (i = 0; i < syntax.errors.length; ++i) {
                    err = syntax.errors[i];
                    $("#errorarea").append("<li class='list-group-item'>"+err.message+"</li><br>");
                  }
                  item.remove();
                item.error=true;
                $("#fileInput").val('');
                return false;
                }else{
                  var result=syntax.body;
                  var i;
                  var j=0;
                  var k=0;
                  var resultset=[];
                  function A(callback){
                    for(i=0;i<result.length;i++ ){
                    	if(result[i].type=='FunctionDeclaration'){
                    		if(result[i].id.name==fname){
                    			j++;
                    		}else{
                    			delete result[i];
                    		}
                    	}else{
                    		resultset[k]=result[i];
                    		k++;
                    	}
                    }
                    callback();
                  }
                  function B(callback2){
                    setTimeout(function(){
                      if(resultset.length>0 && j==0){
                        for(i=1;i<resultset.length; i++){
                      		if(resultset[i].declarations[0].init.type=='FunctionExpression' && resultset[i].declarations[0].id.name==fname){
                      			j++;
                      		}
                        }
                      }
                      callback2();
                    },200);
                  }
                  A(function(){
                    B(function(){
                      setTimeout(function(){
                        if(j > 0){
                          item.error=false;
                          return  true;
                        }else{
                          $("#errorarea").append("<li class='list-group-item'>Function name doesnot match.</li><br>");
                          item.remove();
                          item.error=true;
                          return  false;
                        }
                      },400);
                    });
                  });
                }
              } catch (e) {
                $("#errorarea").append("<li class='list-group-item'>"+e.message+"</li>");
                item.remove();
                item.error=true;
                return false;
              }
            }
          }
            reader.readAsText(file);
            return reed();
      }else{
        $("#errorarea").html("<li class='list-group-item'>Please upload a JS file.</li>");
        return false;
      }
    };

    // --------------------
    uploader.onBeforeUploadItem = function(item) {
      if(item.error==true){
        alert("Remove the errors");
        return false;
      }
      console.info('Before upload', item);
    };
    // --------------------
    uploader.onSuccessItem = function(item, response, status, headers) {
     if(response=='Error in file'){
       $("#errorarea").html("<li class='list-group-item'>"+response+"</li>");
       item.remove();
       $scope.functioname=false;
       return false;
     }else{
       $scope.filename=response.name;
       $scope.functioname=true;
       $("#errorarea").html("<li class='list-group-item'>File uploaded Successfully</li>");
     }
    };
    // --------------------

    /* End */

    $scope.submitForm = function() {
      var restrict_login;
      var max_attempts;
      var attempt_interval;
      var block_time;
      var fields=JSON.stringify($scope.api.fields);
      if(fields==undefined){
        $scope.auth.parameter="Parameter field required.";
        $scope.auth.paramerroricon="fa fa-warning text-danger";
        return false;
      }
      if($scope.selectedmodel==undefined){
        $scope.auth.app="Please create a App to add auth .";
        $scope.auth.apperroricon="fa fa-warning text-danger";
        return false;
      }
      if($scope.filename==''){
        $scope.auth.filerror="Please Upload a js file.";
        $scope.auth.filerroricon="fa fa-warning text-danger";
        return false;
      }
      if($scope.auth.restrict_login==true){
        if($scope.auth.max_attempts<0 || $scope.auth.attempt_interval<0 || $scope.auth.block_time<0){
          $scope.auth.loginattempterror="Please give a proper input";
          $scope.auth.loginattempterroricon="fa fa-warning text-danger";
          return false;
        }else{
          restrict_login=1;
          max_attempts=$scope.auth.max_attempts;
          attempt_interval=$scope.auth.attempt_interval;
          block_time=$scope.auth.block_time;
        }
      }else{
        restrict_login=0;
        max_attempts=0;
        attempt_interval=0;
        block_time=0;
      }
      Auth
        .add({
          name: $scope.api.name,
          method: $scope.api.method,
          file_name:$scope.filename,
          function_name:$scope.auth.function_name,
          uri: $scope.api.uri,
          fields: fields,
          content_type:$scope.api.content_type,
          restrict_login:restrict_login,
          max_attempts:max_attempts,
          block_time:block_time,
          attempt_interval:attempt_interval,
          modelname:$scope.selectedmodel.name,
          modelid:$scope.selectedmodel.id,
          client_id:$scope.selectedmodel.client_id,
          content_type: $scope.api.content_type
        })
        .$promise
        .then(function() {
          $state.go('app.auth-list');
        });
    };
  }else{
    $state.go('login');
  }

}])

.controller('EditAuthController', ['$scope', 'Api', 'Model','Auth','$q',
    '$stateParams', '$state','$cookieStore','FileUploader', function($scope,  Api, Model, Auth, $q,
    $stateParams, $state,$cookieStore,FileUploader) {
    var currentUser=$cookieStore.get('cookiesvalue');
    $scope.action = 'Save';
    $scope.models = [];
    $scope.selectedmodel;
    $scope.api = {};
    $scope.isDisabled = true;
    if(currentUser) {
    $q.all([
      Model.find().$promise,
      Api.findById({ id: $stateParams.id }).$promise,
      Auth.find({filter:{where:{api_id: $stateParams.id}}}).$promise
    ])
    .then(function(data) {
      var models = $scope.models = data[0];
      $scope.api = data[1];
      $scope.selectedmodel;
      var auth= data[2];
      $scope.auth =auth[0];
      if($scope.auth.restrict_login==0){
        $scope.disablemax_attempts=true;
        $scope.disableattempt_interval=true;
        $scope.disableblock_time=true;
      }
      $scope.auth.restrict_login=($scope.auth.restrict_login==1)?true:false;
      $scope.auth.filename=$scope.auth.file_name;
      var fields = JSON.parse(data[1].fields);
      var allfields = Object.keys(fields).map(function(k) {
          return fields[k]
      });
      var fieldname;
      var fieldobj={};
      allfields.forEach(function(fieldname, index) {
          fieldname = fieldname.split("!!");
          fieldobj[fieldname[0]]=fieldname[1];
      });
      $scope.api.fields=fieldobj;
      var selectedmodelIndex = models
        .map(function(modeldata) {
          return modeldata.id;
        })
        .indexOf($scope.api.model_id);
      $scope.selectedmodel = models[selectedmodelIndex];
    });
    $scope.obj = {
      options: {
        mode: 'code'
      }
      };
    /* JS File upload validation */

    /* Initialize the uploader directory */
    var uploader = $scope.uploader = new FileUploader({
      scope: $scope,                          // to automatically update the html. Default: $rootScope
      url: '/api/containers/container1/upload',
      formData: [
        { key: 't' }
      ]
    });
    /* End */
    $scope.functioname=false;
    uploader.filters.push({
        name: 'customFilter',
        fn: function (item, options) { // second user filter
          var fname=document.getElementById('fname').value;
          if(fname==''){
            alert("Please provide the function name first.")
            return false
          }else{
            return true;
          }

        }
    });
    // REGISTER HANDLERS
    // --------------------
    uploader.onAfterAddingFile = function(item) {
      var fileInput = document.getElementById('fileInput');
      var fname=document.getElementById('fname').value;
      item.file.name=Math.floor((Math.random() * 10000000000) + 1)+"-"+fname+".js";
      if(item.file.type=='application/x-javascript' || item.file.type=='application/javascript'){
      $("#errorarea").html("");
        var file = fileInput.files[0];
          var reader = new FileReader();
          var reed= function(){
             reader.onload = function(e) {
              var content, syntax, i, err;
              try {
                content = "'use strict'; "+reader.result;
                syntax = esprima.parse(content, { tolerant: true});
                if(syntax.errors.length>0){
                  for (i = 0; i < syntax.errors.length; ++i) {
                    err = syntax.errors[i];
                    $("#errorarea").append("<li>"+err.message+"</li><br>");
                  }
                item.remove();
                item.error=true;
                return false;
                }else{
                  var result=syntax.body;
                  var i;
                  var j=0;
                  var k=0;
                  var resultset=[];
                  function A(callback){
                    for(i=0;i<result.length;i++ ){
                      if(result[i].type=='FunctionDeclaration'){
                        if(result[i].id.name==fname && result[i].params.length==3){
                          j++;
                        }else{
                          delete result[i];
                        }
                      }else{
                        resultset[k]=result[i];
                        k++;
                      }
                    }
                    callback();
                  }
                  function B(callback2){
                    setTimeout(function(){
                      if(resultset.length>0 && j==0){
                        for(i=1;i<resultset.length; i++){
                          if(resultset[i].declarations[0].init.type=='FunctionExpression'&& resultset[i].declarations[0].id.name==fname && resultset[i].declarations[0].init.params.length==3){
                            j++;
                          }
                        }
                      }
                      callback2();
                    },200);
                  }
                  A(function(){
                    B(function(){
                      setTimeout(function(){
                        if(j > 0){
                          item.error=false;
                          return  true;
                        }else{
                          $("#errorarea").append("<li>Function name doesnot match or the function doesnot contain valid parameters.</li><br>");
                          item.remove();
                          item.error=true;
                          return  false;
                        }
                      },400);
                    });
                  });
                }
              } catch (e) {
                $("#errorarea").append("<li>"+e.message+"</li>");
                item.remove();
                item.error=true;
                return false;
              }
            }
          }
            reader.readAsText(file);
            return reed();
      }else{
        $("#errorarea").html("<li class='list-group-item'>Please upload a JS file.</li>");
        return false;
      }
    };
    // --------------------
    uploader.onBeforeUploadItem = function(item) {
      if(item.error==true){
        alert("Remove the errors");
        return false;
      }
    };
    // --------------------
    uploader.onSuccessItem = function(item, response, status, headers) {
     if(response=='Error in file'){
       $("#errorarea").html("<li>"+response+"</li>");
       item.remove();
       $scope.functioname=false;
       return false;
     }else{
       $scope.auth.filename=response.name;
       $scope.functioname=true;
       $("#errorarea").html("<li>File uploaded Successfully</li>");
     }
    };
    // --------------------
    /* End */
    $scope.toggle_fields=function(){
      var restrict_login = ($scope.auth.restrict_login==true)?1:0;
      if(restrict_login==1){
        $scope.disablemax_attempts=false;
        $scope.disableattempt_interval=false;
        $scope.disableblock_time=false;
      }else{
        $scope.disablemax_attempts=true;
        $scope.disableattempt_interval=true;
        $scope.disableblock_time=true;
        $scope.auth.max_attempts='';
        $scope.auth.attempt_interval='';
        $scope.auth.block_time='';
      }
    }
    $scope.submitForm = function() {
      var restrict_login;
      var max_attempts;
      var attempt_interval;
      var block_time;
      var fields=JSON.stringify($scope.api.fields);
      if(fields==undefined){
        $scope.auth.parameter="Parameter field required.";
        $scope.auth.paramerroricon="fa fa-warning text-danger";
        return false;
      }
      if($scope.auth.filename==''){
        $scope.auth.filerror="Please Upload a js file.";
        $scope.auth.filerroricon="fa fa-warning text-danger";
        return false;
      }
      if($scope.auth.restrict_login==true){
        if($scope.auth.max_attempts<0 || $scope.auth.attempt_interval<0 || $scope.auth.block_time<0){
          $scope.auth.loginattempterror="Please give a proper input";
          $scope.auth.loginattempterroricon="fa fa-warning text-danger";
          return false;
        }else{
          restrict_login=1;
          max_attempts=$scope.auth.max_attempts;
          attempt_interval=$scope.auth.attempt_interval;
          block_time=$scope.auth.block_time;
        }
      }else{
        restrict_login=0;
        max_attempts=0;
        attempt_interval=0;
        block_time=0;
      }
      $scope.auth.max_attempts=max_attempts;
      $scope.auth.attempt_interval=attempt_interval;
      $scope.auth.restrict_login=restrict_login;
      $scope.auth.block_time=block_time;
      $scope.api.fields = fields;
      $scope.api.model_id = $scope.selectedmodel.id;
      $scope.api.modelname = $scope.selectedmodel.name;
      $scope.auth.file_name=$scope.auth.filename;
      $scope.auth
        .$save()
        .then(function(auth) {
          $scope.api
            .$save()
            .then(function(api) {
              $state.go('app.auth-list');
            });
        });
    };
  }else{
      $state.go('login');
  }
}])
