angular
.module('admin.auth')
.controller('AuthlistController', ['$scope','Api','Model','Auth','$cookieStore', '$filter', '$state','$window','$uibModal', function($scope, Api,Model,Auth,$cookieStore, $filter, $state,$window,$uibModal) {
  var currentUser=$cookieStore.get('cookiesvalue');
  if(currentUser) {
    $scope.client_id=currentUser.id;
    $scope.datas = [];
    $scope.tempDatasArr = [];

    $scope.maxSize = 5;
    $scope.bigTotalItems;
    $scope.itemsPerPage = 8;
    $scope.bigCurrentPage = 1;
    $scope.begin = 0;

    $scope.search = function(val) {
      $scope.bigCurrentPage = 1;
      $scope.begin = 0;
      $scope.datas = $filter('filter')($scope.tempDatasArr,val);
    }

    $scope.$watch('datas',function(newVal) {
      $scope.bigTotalItems = newVal.length;
    })
    var init = function() {
      Auth.find({filter:{where:{client_id:$scope.client_id},include:['model','api']}}).$promise.then(function(response) {
        $scope.auths = response.reverse();
        $scope.tempDatasArr = $scope.auths;
        $scope.datas = $scope.tempDatasArr;
        $scope.bigTotalItems = $scope.datas.length;
      });
    }
    init();
    $scope.Delete_Auth=function(id,model_id){
      $scope.message = "Are you absolutely sure you want to delete?";
      var modalInstance = $uibModal.open({
          animation: true,
          scope : $scope,
          templateUrl: 'views/modal.html',
          controller: 'ModalInstanceCtrl'
      });
      modalInstance.result.then(function (status) {
           if(status) {
             Api.updateAll({where:{model_id:model_id}},{attribute:1,token:0},function(err,info){
               Api
                 .deleteById({id:id})
                 .$promise
                 .then(function() {
                   Api.find({filter:{where:{is_logout:1,model_id:model_id}}})
                     .$promise
                     .then(function(response){
                       Api.deleteById({ id: response[0].id });
                       $window.location.reload();
                     });
                 });
             });
           }else {
             return false;
           }
       },function(){
          return false;
       });
    };
  }else {
    $state.go('login');
  }

}])
.controller('AuthConfigureController', ['$scope','Auth' ,'Model', 'Api',
    '$state','$cookieStore','Upload','$timeout', function($scope, Auth, Model, Api, $state,$cookieStore,Upload,$timeout) {
  var currentUser=$cookieStore.get('cookiesvalue');
  if(currentUser){
    $scope.client_id=currentUser.id;
    $scope.action = 'Add';
    $scope.models = [];
    $scope.selectedmodel;
    $scope.isDisabled = false;
    var model=[];
    Auth.find({filter:{fields:{model_id:true},where:{client_id:$scope.client_id}}}).$promise.then(function(model) {
        model.forEach(function(model_id, index) {
            model[index]=model_id.model_id;
        });
        Model
          .find({filter:{where:{id:{nin:model},client_id:$scope.client_id}}})
          .$promise
          .then(function(models) {
          $scope.models = models;
          $scope.selectedmodel = $scope.selectedmodel || models[0];
          });
    });
    var json = {};
    $scope.obj = {
      data: json,
      options: {
        mode: 'code'
      }
      };
    $scope.onLoad = function (instance) {
        instance.expandAll();
    };
    $scope.uploadPic = function(file) {
      alert(1);
        console.log(file);
        file.upload = Upload.upload({
          url: 'client/files',
          data: {username: $scope.username, file: file},
        });
        file.upload.then(function (response) {
          $timeout(function () {
            file.result = response.data;
          });
        }, function (response) {
          if (response.status > 0)
            $scope.errorMsg = response.status + ': ' + response.data;
        }, function (evt) {
          // Math.min is to fix IE which reports 200% sometimes
          file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });
    }

    $scope.submitForm = function() {
      var fields=JSON.stringify($scope.api.fields);
      if(fields==undefined){
        $scope.auth.parameter="Parameter field required.";
        $scope.auth.paramerroricon="fa fa-warning text-danger";
        return false;
      }
      if($scope.selectedmodel==undefined){
        $scope.auth.app="Please create a App to add auth .";
        $scope.auth.apperroricon="fa fa-warning text-danger";
        return false;
      }
      $scope.auth.status_string='statusCode';
      $scope.auth.status_success_code=200;
      Auth
        .add({
          name: $scope.api.name,
          method: $scope.api.method,
          primary_id:$scope.auth.primary_id,
          status_string:$scope.auth.status_string,
          status_success_code:$scope.auth.status_success_code,
          status_failure_code:$scope.auth.status_failure_code,
          uri: $scope.api.uri,
          fields: fields,
          modelname:$scope.selectedmodel.name,
          modelid:$scope.selectedmodel.id,
          client_id:$scope.selectedmodel.client_id
        })
        .$promise
        .then(function() {
          $state.go('app.auth-list');
        });
    };
  }else{
    $state.go('login');
  }

}])

.controller('EditAuthController', ['$scope', 'Api', 'Model','Auth','$q',
    '$stateParams', '$state','$cookieStore', function($scope,  Api, Model, Auth, $q,
    $stateParams, $state,$cookieStore) {
    var currentUser=$cookieStore.get('cookiesvalue');
    $scope.action = 'Save';
    $scope.models = [];
    $scope.selectedmodel;
    $scope.api = {};
    $scope.isDisabled = true;
    if(currentUser) {
    $q.all([
      Model.find().$promise,
      Api.findById({ id: $stateParams.id }).$promise,
      Auth.find({filter:{where:{api_id: $stateParams.id}}}).$promise
    ])
    .then(function(data) {
      var models = $scope.models = data[0];
      $scope.api = data[1];
      $scope.selectedmodel;
      var auth= data[2];
      $scope.auth =auth[0];
      var fields = JSON.parse(data[1].fields);
      var allfields = Object.keys(fields).map(function(k) {
          return fields[k]
      });
      var fieldname;
      var fieldobj={};
      allfields.forEach(function(fieldname, index) {
          fieldname = fieldname.split("!!");
          fieldobj[fieldname[0]]=fieldname[1];
      });
      $scope.api.fields=fieldobj;
      var selectedmodelIndex = models
        .map(function(modeldata) {
          return modeldata.id;
        })
        .indexOf($scope.api.model_id);
      $scope.selectedmodel = models[selectedmodelIndex];
    });
    $scope.obj = {
      options: {
        mode: 'code'
      }
      };
    $scope.onLoad = function (instance) {
        instance.expandAll();
    };
    $scope.submitForm = function() {
      var fields=JSON.stringify($scope.api.fields);
      if(fields==undefined){
        $scope.auth.parameter="Parameter field required.";
        $scope.auth.paramerroricon="fa fa-warning text-danger";
        return false;
      }
      $scope.api.fields = fields;
      $scope.api.model_id = $scope.selectedmodel.id;
      $scope.api.modelname = $scope.selectedmodel.name;
      $scope.auth
        .$save()
        .then(function(auth) {
          $scope.api
            .$save()
            .then(function(api) {
              $state.go('app.auth-list');
            });
        });
    };
  }else{
      $state.go('login');
  }
}])
