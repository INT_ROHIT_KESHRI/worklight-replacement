angular
.module('admin.ratelimiter', [
  'ui.router',
  'lbServices',
  'ngCookies',
  'ui.bootstrap',

  'SmartAdmin.Layout',
  'SmartAdmin.UI'
])
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/all-caches');
  $stateProvider
  .state('app.all-ratelimiters', {
      url: '/all-ratelimiters',
      views: {
          "content@app": {
              templateUrl: 'views/RATELIMITER/all-ratelimiters.html',
              controller: 'AllRatelimitersController'
          }
      },
      data:{
          title: 'All RATELIMITER'
      }
  })
  .state('app.add-ratelimiter', {
      url: '/add-ratelimiter',
      views: {
          "content@app": {
              templateUrl: 'views/RATELIMITER/ratelimiter-form.html',
              controller: 'AddRatelimiterController'
          }
      },
      data:{
          title: 'Add RATELIMITER'
      }
  })
  .state('app.edit-ratelimiter', {
      url: '/edit-ratelimiter/:id',
      views: {
          "content@app": {
              templateUrl: 'views/RATELIMITER/ratelimiter-form.html',
              controller: 'EditRatelimiterController'
          }
      },
      data:{
          title: 'Edit RATELIMITER'
      }
  })
  .state('app.delete-ratelimiter', {
      url: '/delete-ratelimiter/:id',
      views: {
          "content@app": {
              controller: 'DeleteRatelimiterController'
          }
      }
  })
}])
