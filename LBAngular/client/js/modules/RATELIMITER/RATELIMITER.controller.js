angular
.module('admin.ratelimiter')
.controller('AllRatelimitersController', ['$scope', 'Api','Ratelimiter','$cookieStore', '$filter', '$state','$window','$uibModal', function($scope,Api,Ratelimiter,$cookieStore, $filter, $state,$window,$uibModal) {
  var currentUser=$cookieStore.get('cookiesvalue');
  if(currentUser) {
    $scope.client_id=currentUser.id;
    $scope.datas = [];
    $scope.tempDatasArr = [];
    var api_id=[];
    var api_details=[];

    $scope.maxSize = 5;
    $scope.bigTotalItems;
    $scope.itemsPerPage = 8;
    $scope.bigCurrentPage = 1;
    $scope.begin = 0;

    $scope.search = function(val) {
      $scope.bigCurrentPage = 1;
      $scope.begin = 0;
      $scope.datas = $filter('filter')($scope.tempDatasArr,val);
    }

    $scope.$watch('datas',function(newVal) {
      $scope.bigTotalItems = newVal.length;
    })
    var init = function() {
      Ratelimiter.find({filter:{where:{client_id:$scope.client_id},include:['api']}}).$promise.then(function(response) {
          $scope.ratelimiters = response.reverse();
          $scope.tempDatasArr = $scope.ratelimiters;
          $scope.datas = $scope.tempDatasArr;
          $scope.bigTotalItems = $scope.datas.length;
      });
    }
    init();
    $scope.Status_Update=function(flag,id){
      flag=(flag==1)?0:1;
      Ratelimiter.prototype$updateAttributes({id:id},{statuschange:1,flag:flag},function(success){
        $window.location.reload();
      },function(err){
        alert("Unable to update.Please try again latter");
        return false;
      });
    };
    $scope.Delete_Ratelimiter=function(id){
      $scope.message = "Are you absolutely sure you want to delete?";
      var modalInstance = $uibModal.open({
          animation: true,
          scope : $scope,
          templateUrl: 'views/modal.html',
          controller: 'ModalInstanceCtrl'
      });
      modalInstance.result.then(function (status) {
           if(status) {
             Ratelimiter
               .deleteById({ id:id })
               .$promise
               .then(function() {
                $window.location.reload();
               });
           }else {
             return false;
           }
       },function(){
        return false;
       });
    };
  }else {
    $state.go('login');
  }

}])
.controller('AddRatelimiterController', ['$scope', 'Ratelimiter', 'Auth','Api','Client','$cookieStore',
    '$state', function($scope, Ratelimiter, Auth, Api , Client , $cookieStore, $state) {
  var currentUser=$cookieStore.get('cookiesvalue');
  if(currentUser) {
  var api_id=[];
  $scope.client_id=currentUser.id;
  $scope.action = 'Add';
  $scope.apis = [];
  $scope.selectedapi;
  $scope.isDisabled = false;
  $scope.ratelimiter_ascess=false;
  Client.getCurrent(function(value,headers){
    var ascess_level=JSON.parse(value.ascess_level);
    if(ascess_level.ratelimiter && ascess_level.ratelimiter==true){
      $scope.ratelimiter_ascess=true;
    }
  });
  Ratelimiter
    .find()
    .$promise
    .then(function(ratelimiters) {
      angular.forEach(ratelimiters, function (ratelimiter, ratelimiterIndex) {
        api_id[ratelimiterIndex]=ratelimiter.api_id;
      });
      var A = function(callback) {
        Auth.find({filter:{where:{client_id:$scope.client_id}}},function(authresponse,authheader){
            if(authresponse[0]){
              angular.forEach(authresponse, function(value, key) {
                api_id.push(value.api_id);
              });
            }
          });
          callback();
      }
      var B=function(){
        setTimeout(function(){
        Api
          .find({filter:{where:{is_logout:{neq:1},client_id:$scope.client_id,id:{nin:api_id},flag:1}}})
          .$promise
          .then(function(apis) {
          $scope.apis = apis;
          $scope.selectedapi = $scope.selectedapi || apis[0];
        });
      },500);
      }
    A(function() {
      B();
    });
    });
  $scope.submitForm = function() {
    if($scope.ratelimiter_ascess==false){
      $scope.apierror="You are not eligable to ascess the ratelimiter features.Contact the administrator.";
      $scope.apierroricon="fa fa-warning text-danger";
      return false;
    }
    if($scope.selectedapi==undefined){
      $scope.apierror="Please add a api to add ratelimiter";
      $scope.apierroricon="fa fa-warning text-danger";
      return false;
    }
    if($scope.ratelimiter.hits<=5){
      $scope.hitserror="The number of hits should be more then 5";
      $scope.hitserroricon="fa fa-warning text-danger";
      return false;
    }
    if($scope.ratelimiter.exp==undefined){
      $scope.error="This Duration Should be more then 30 seconds";
      $scope.durationerroricon="fa fa-warning text-danger";
      return false;
    }else if(($scope.ratelimiter.exp.mins==undefined || $scope.ratelimiter.exp.mins==0) && ($scope.ratelimiter.exp.secs==undefined || $scope.ratelimiter.exp.secs<30)){
      $scope.error="This Duration Should be more then 30 seconds";
      $scope.durationerroricon="fa fa-warning text-danger";
      return false;
    }else if($scope.ratelimiter.exp.secs==undefined && $scope.ratelimiter.exp.mins>0){
      $scope.ratelimiter.expiretime=($scope.ratelimiter.exp.mins*60);
    }else if(($scope.ratelimiter.exp.mins==undefined||$scope.ratelimiter.exp.mins==0) && $scope.ratelimiter.exp.secs>30){
        $scope.ratelimiter.expiretime=$scope.ratelimiter.exp.secs;
    }else{
      $scope.ratelimiter.expiretime=(parseInt($scope.ratelimiter.exp.mins*60)+parseInt($scope.ratelimiter.exp.secs));
    }
    Ratelimiter
      .create({
        api_id: $scope.selectedapi.id,
        time: $scope.ratelimiter.expiretime,
        client_id:$scope.client_id,
        hits:$scope.ratelimiter.hits
      })
      .$promise
      .then(function() {
        $state.go('app.all-ratelimiters');
      });
  };
}else{
      $state.go('login');
}
}])
.controller('EditRatelimiterController', ['$scope', 'Api', 'Ratelimiter','$q',
    '$stateParams', '$cookieStore','$state', function($scope,  Api, Ratelimiter,$q,
    $stateParams,$cookieStore, $state) {
    var currentUser=$cookieStore.get('cookiesvalue');
    if(currentUser){
      $scope.client_id=currentUser.id;
      $scope.action = 'Save';
      $scope.apis = [];
      $scope.selectedapi;
      $scope.ratelimiter = {};
      $scope.isDisabled = true;
      $q.all([
        Api.find({where:{client_id:$scope.client_id}}).$promise,
        Ratelimiter.findById({ id: $stateParams.id }).$promise
      ]).then(function(data) {
        var apis = $scope.apis = data[0];
        $scope.ratelimiter = data[1];
        var selectedapiIndex = apis
          .map(function(apidata) {
            return apidata.id;
          })
          .indexOf($scope.ratelimiter.api_id);
        $scope.selectedapi = apis[selectedapiIndex];
        $scope.ratelimiter.duration=($scope.ratelimiter.duration/ 1000);
        var mins=Math.floor($scope.ratelimiter.duration/ 60);
        var secs=(mins==0)?$scope.ratelimiter.duration:$scope.ratelimiter.duration-mins*60;
        $scope.ratelimiter.exp={};
        $scope.ratelimiter.exp.mins=parseInt(mins);
        $scope.ratelimiter.exp.secs=parseInt(secs);
      });
      $scope.submitForm = function() {
        if($scope.ratelimiter.hits<=5){
          $scope.hitserror="The number of hits should be more then 5";
          $scope.hitserroricon="fa fa-warning text-danger";
          return false;
        }
        if($scope.ratelimiter.exp==undefined){
          $scope.error="This Duration Should be more then 30 seconds";
          $scope.durationerroricon="fa fa-warning text-danger";
          return false;
        }else if(($scope.ratelimiter.exp.mins==undefined || $scope.ratelimiter.exp.mins==0) && ($scope.ratelimiter.exp.secs==undefined || $scope.ratelimiter.exp.secs<30)){
          $scope.error="This Duration Should be more then 30 seconds";
          $scope.durationerroricon="fa fa-warning text-danger";
          return false;
        }else if($scope.ratelimiter.exp.secs==undefined && $scope.ratelimiter.exp.mins>0){
          $scope.ratelimiter.expiretime=($scope.ratelimiter.exp.mins*60);
        }else if(($scope.ratelimiter.exp.mins==undefined||$scope.ratelimiter.exp.mins==0) && $scope.ratelimiter.exp.secs>30){
          $scope.ratelimiter.expiretime=$scope.ratelimiter.exp.secs;
        }else{
          $scope.ratelimiter.expiretime=(parseInt($scope.ratelimiter.exp.mins*60)+parseInt($scope.ratelimiter.exp.secs));
        }
        $scope.ratelimiter.api_id = $scope.selectedapi.id;
        $scope.ratelimiter
          .$save()
          .then(function() {
            $state.go('app.all-ratelimiters');
          });
      };
    }else{
      $state.go('login');
    }
}])
