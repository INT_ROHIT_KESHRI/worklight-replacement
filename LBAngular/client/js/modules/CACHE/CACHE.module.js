angular
.module('admin.cache', [
  'ui.router',
  'lbServices',
  'ngCookies',
  'ui.bootstrap',

  'SmartAdmin.Layout',
  'SmartAdmin.UI'
])
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/all-caches');
  $stateProvider
  .state('app.all-caches', {
      url: '/all-caches',
      views: {
          "content@app": {
              templateUrl: 'views/CACHE/all-caches.html',
              controller: 'AllCachesController'
          }
      },
      data:{
          title: 'All CACHES'
      }
  })
  .state('app.add-cache', {
      url: '/add-cache',
      views: {
          "content@app": {
              templateUrl: 'views/CACHE/cache-form.html',
              controller: 'AddCacheController'
          }
      },
      data:{
          title: 'Add CACHE'
      }
  })
  .state('app.edit-cache', {
      url: '/edit-cache/:id',
      views: {
          "content@app": {
              templateUrl: 'views/CACHE/cache-form.html',
              controller: 'EditCacheController'
          }
      },
      data:{
          title: 'Edit CACHE'
      }
  })
  .state('app.delete-cache', {
      url: '/delete-cache/:id',
      views: {
          "content@app": {
              controller: 'DeleteCacheController'
          }
      }
  })
}])
