angular
.module('admin.cache')
.controller('AllCachesController', ['$scope', 'Api','Cache','$cookieStore', '$filter', '$state','$window','$uibModal', function($scope,Api,Cache,$cookieStore, $filter, $state,$window,$uibModal) {
  var currentUser=$cookieStore.get('cookiesvalue');
  if(currentUser) {
    $scope.client_id=currentUser.id;
    $scope.datas = [];
    $scope.tempDatasArr = [];
    var api_id=[];
    var api_details=[];

    $scope.maxSize = 5;
    $scope.bigTotalItems;
    $scope.itemsPerPage = 8;
    $scope.bigCurrentPage = 1;
    $scope.begin = 0;

    $scope.search = function(val) {
      $scope.bigCurrentPage = 1;
      $scope.begin = 0;
      $scope.datas = $filter('filter')($scope.tempDatasArr,val);
    }

    $scope.$watch('datas',function(newVal) {
      $scope.bigTotalItems = newVal.length;
    })
    var init = function() {
      Cache.find({filter:{where:{client_id:$scope.client_id},include:['api']}}).$promise.then(function(response) {
          $scope.caches = response.reverse();
          $scope.tempDatasArr = $scope.caches;
          $scope.datas = $scope.tempDatasArr;
          $scope.bigTotalItems = $scope.datas.length;
      });
    }
    init();
    $scope.Status_Update=function(flag,id){
      flag=(flag==1)?0:1;
      Cache.prototype$updateAttributes({id:id},{flag:flag},function(success){
        $window.location.reload();
      },function(err){
        alert("Unable to update.Please try again latter");
        return false;
      });
    };
    $scope.Delete_Cache=function(id){
      $scope.message = "Are you absolutely sure you want to delete?";
      var modalInstance = $uibModal.open({
          animation: true,
          scope : $scope,
          templateUrl: 'views/modal.html',
          controller: 'ModalInstanceCtrl'
      });
      modalInstance.result.then(function (status) {
           if(status) {
             Cache
               .deleteById({ id:id })
               .$promise
               .then(function() {
                $window.location.reload();
               });
           }else {
             return false;
           }
       },function(){
         return false;
       });
    };
  }else {
    $state.go('login');
  }

}])
.controller('AddCacheController', ['$scope', 'Cache', 'Api','Auth','Client','$cookieStore',
    '$state', function($scope, Cache, Api, Auth, Client , $cookieStore, $state) {
  var currentUser=$cookieStore.get('cookiesvalue');
  var api_id=[];
  $scope.client_id=currentUser.id;
  $scope.action = 'Add';
  $scope.apis = [];
  $scope.selectedapi;
  $scope.isDisabled = false;
  $scope.cache={};
  $scope.cache_ascess=false;
  if(currentUser) {
    Client.getCurrent(function(value,headers){
      var ascess_level=JSON.parse(value.ascess_level);
      if(ascess_level.cache && ascess_level.cache==true){
        $scope.cache_ascess=true;
      }
    });
  Cache
    .find()
    .$promise
    .then(function(caches) {
      angular.forEach(caches, function (cache, cachIndex) {
        api_id[cachIndex]=cache.api_id;
      });
        var A = function(callback) {
          Auth.find({filter:{where:{client_id:$scope.client_id}}},function(authresponse,authheader){
              if(authresponse[0]){
                angular.forEach(authresponse, function(value, key) {
                  api_id.push(value.api_id);
                });
              }
            });
            callback();
        }
        var B=function(){
          setTimeout(function(){
          Api
            .find({filter:{where:{is_logout:{neq:1},client_id:$scope.client_id,id:{nin:api_id},flag:1}}})
            .$promise
            .then(function(apis) {
            $scope.apis = apis;
            $scope.selectedapi = $scope.selectedapi || apis[0];
          });
        },500);
        }
      A(function() {
        B();
      });
    });
      $scope.submitForm = function() {
        if($scope.cache_ascess==false){
          $scope.apierror="You are not eligable to ascess the cache features.Contact the administrator.";
          $scope.apierroricon="fa fa-warning text-danger";
          return false;
        }
        if($scope.selectedapi==undefined){
          $scope.apierror="Please add a api to add caching";
          $scope.apierroricon="fa fa-warning text-danger";
          return false;
        }
        if($scope.cache.duration==undefined){
          $scope.error="This Duration Should be more then 30 seconds";
          $scope.durationerroricon="fa fa-warning text-danger";
          return false;
        }else if(($scope.cache.duration.mins==undefined || $scope.cache.duration.mins==0) && ($scope.cache.duration.secs==undefined || $scope.cache.duration.secs<30)){
          $scope.error="This Duration Should be more then 30 seconds";
          $scope.durationerroricon="fa fa-warning text-danger";
          return false;
        }else if($scope.cache.duration.secs==undefined && $scope.cache.duration.mins>0){
          $scope.cache.duration=($scope.cache.duration.mins*60);
        }else if(($scope.cache.duration.mins==undefined||$scope.cache.duration.mins==0) && $scope.cache.duration.secs>30){
          $scope.cache.duration=$scope.cache.duration.secs;
        }else{
          $scope.cache.duration=(parseInt($scope.cache.duration.mins*60)+parseInt($scope.cache.duration.secs));
        }
        Cache
          .create({
            api_id: $scope.selectedapi.id,
            client_id:$scope.client_id,
            duration: $scope.cache.duration
          })
          .$promise
          .then(function() {
            $state.go('app.all-caches');
          });
      };
    }else{
      $state.go('login');
    }

}])

.controller('EditCacheController', ['$scope', 'Api', 'Cache','$q',
    '$stateParams', '$cookieStore','$state', function($scope,  Api, Cache,$q,
    $stateParams,$cookieStore, $state) {
    var currentUser=$cookieStore.get('cookiesvalue');
    $scope.client_id=currentUser.id;
    $scope.action = 'Save';
    $scope.apis = [];
    $scope.selectedapi;
    $scope.cache = {};
    $scope.isDisabled = true;
    if(currentUser){
    $q.all([
      Api.find({where:{client_id:$scope.client_id}}).$promise,
      Cache.findById({ id: $stateParams.id }).$promise
    ]).then(function(data) {
      var apis = $scope.apis = data[0];
      $scope.cache = data[1];
      var selectedapiIndex = apis
        .map(function(apidata) {
          return apidata.id;
        })
        .indexOf($scope.cache.api_id);
      $scope.selectedapi = apis[selectedapiIndex];
      var mins=Math.floor($scope.cache.duration/ 60);
      var secs=(mins==0)?$scope.cache.duration:$scope.cache.duration-mins*60;
      $scope.cache.duration={};
      $scope.cache.duration.mins=parseInt(mins);
      $scope.cache.duration.secs=parseInt(secs);
    });

      $scope.submitForm = function() {
        if($scope.cache.duration==undefined){
          $scope.error="This Duration Should be more then 30 seconds";
          $scope.durationerroricon="fa fa-warning text-danger";
          return false;
        }else if(($scope.cache.duration.mins==undefined || $scope.cache.duration.mins==0) && ($scope.cache.duration.secs==undefined || $scope.cache.duration.secs<30)){
          $scope.error="This Duration Should be more then 30 seconds";
          $scope.durationerroricon="fa fa-warning text-danger";
          return false;
        }else if($scope.cache.duration.secs==undefined && $scope.cache.duration.mins>0){
          $scope.cache.duration=($scope.cache.duration.mins*60);
        }else if(($scope.cache.duration.mins==undefined||$scope.cache.duration.mins==0) && $scope.cache.duration.secs>30){
          $scope.cache.duration=$scope.cache.duration.secs;
        }else{
          $scope.cache.duration=(parseInt($scope.cache.duration.mins*60) +  parseInt($scope.cache.duration.secs));
        }
        $scope.cache.api_id = $scope.selectedapi.id;
        $scope.cache
          .$save()
          .then(function() {
            $state.go('app.all-caches');
          });
      };
    }else{
        $state.go('login');
    }
}])
