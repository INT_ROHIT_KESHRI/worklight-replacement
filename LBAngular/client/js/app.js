angular
  .module('app', [
    'ui.router',
    'lbServices',
    'ngCookies',
    'ui.bootstrap',
    'ng.jsoneditor',
    'angular.filter',
    'SmartAdmin.Layout',
    'SmartAdmin.UI',
    'admin.model',
    'admin.api',
    'admin.cache',
    'admin.ratelimiter',
    'admin.auth',
    'admin.responsetime',
    'admin.restclient',
    'admin.profile',
    'admin.dashboard',
    'angularFileUpload'
  ])
  .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $stateProvider
    .state('login', {
        url: '/login',
        views: {
            "root": {
                templateUrl: 'views/login.html',
                controller: 'AuthLoginController'
            }
        },
        data:{
            title: 'Login'
        }
    })
    .state('adminlogin', {
        url: '/adminlogin',
        views: {
            "root": {
                templateUrl: 'views/adminlogin.html',
                controller: 'AuthAdminLoginController'
            }
        },
        data:{
            title: 'AdminLogin'
        }
    })
    .state('logout', {
        url: 'logout',
        views: {
            "root": {
                controller: 'AuthLogoutController'
            }
        },
        data:{
            title: 'Logout'
        }
    })
    .state('app', {
        abstract: true,
        views: {
            root: {
                templateUrl: 'app/layout/layout.tpl.html'
            }
        }
    })
    .state('forbidden', {
      url: '/forbidden',
      templateUrl: 'views/forbidden.html',
    })
    //Default Routing
    $urlRouterProvider.otherwise('/login');



  }])
  .run(['$rootScope', '$state','$cookieStore','$interval', '$timeout', function($rootScope, $state,$cookieStore,$interval,$timeout) {
    var lastDigestRun = Date.now();
    /*var idleCheck = $interval(function() {
        var now = Date.now();
        if (now - lastDigestRun > 1*60*1000) {
           $state.go('logout');
        }
    }, 60*1000);*/

    var modeChecking = function() {
      if(!$cookieStore.get('cookiesvalue')) {
        $timeout(function() {
          $state.go('login');
        },200);
      }
    }
    modeChecking();
    $rootScope.$on('$stateChangeStart', function(event, next,current) {
      if ($cookieStore.get('cookiesvalue')) {
        var currentUser=$cookieStore.get('cookiesvalue');
        $rootScope.currentUser = {
            id: currentUser.id,
            tokenId: currentUser.tokenId,
            email: currentUser.email,
            timeout:currentUser.timeout,
            realm:currentUser.realm
          };
      }
      lastDigestRun = Date.now();
      if (next.authenticate && !$rootScope.currentUser) {
        event.preventDefault(); //prevent current page from loading
        modeChecking();
      }
    });
  }]);
