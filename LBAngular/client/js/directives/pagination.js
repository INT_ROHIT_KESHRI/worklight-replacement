angular.module('app').directive('pagination', function () {
    return {
        restrict: "E",
        scope : {
            'bigTotalItems' : "=",
            'itemsPerPage' : "=",
            'bigCurrentPage' : '=',
            'maxSize' : "=",
            'begin' : "="
        },
        template : '<uib-pagination total-items="bigTotalItems" items-per-page="itemsPerPage" ng-model="bigCurrentPage" ng-change="pageChanged()" max-size="maxSize" class="pagination-sm" boundary-links="true" num-pages="numPages"></uib-pagination>',
        link: function(scope, element, attributes){

        },
        controller : function($scope, $uibModal, $log, $filter) {
            !$scope.maxSize ? $scope.maxSize = 5 : '';
            $scope.begin ? '' : $scope.begin = 0;
            !$scope.bigCurrentPage ? $scope.bigCurrentPage = 1 : '';
            !$scope.itemsPerPage ? $scope.itemsPerPage = 10 : '';

            $scope.pageChanged = function() {
                $scope.begin = ($scope.bigCurrentPage-1)*$scope.itemsPerPage;
            };
        }
    };
});
