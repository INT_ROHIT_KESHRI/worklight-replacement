angular
  .module('app')
  .controller('AuthLoginController', ['$scope', 'AuthService', '$state','$cookieStore','$rootScope',
      function($scope, AuthService, $state,$cookieStore,$rootScope) {
        if($cookieStore.get('cookiesvalue')) {
          $state.go('app.all-apis');
        }else {
          $scope.user = {
            email: 'suchandra.banerjee@indusnet.co.in',
            password: '12345'
          };
          $scope.login = function() {
            AuthService.login($scope.user.email, $scope.user.password)
              .then(function(data) {
                if(data==false){
                  $scope.error="Wrong Email or Password";
                  $scope.erroricon="fa fa-warning text-danger";
                  return false;
                }else{
                  var now = new Date();
                  var timeout=$rootScope.currentUser.timeout > 0 ? $rootScope.currentUser.timeout:20;
                  var exp = new Date(now.getTime() + timeout*1000);
                  $cookieStore.put('cookiesvalue',$rootScope.currentUser,{'expires': exp});
                  $state.go('app.add-model');
                }

              });
          };
          $scope.resetpassword=function(){
             AuthService.resetpassword($scope.guest.email)
               .then(function(data) {
                 if(data==false){
                   $scope.errorForpass="The email you provide is invalid.";
                   $scope.errorPassicon="fa fa-warning text-danger";
                   return false;
                 }else{
                   $scope.errorForpass="Check your mailbox.";
                   $scope.errorPassicon="fa fa-thumbs-o-up text-success";
                   return false;
                 }
               });
           };
        }

  }])
  .controller('AuthAdminLoginController', ['$scope', 'AuthService','$state','$cookieStore','$rootScope',
      function($scope, AuthService, $state,$cookieStore,$rootScope) {
        $rootScope.currentUser={};
        if($cookieStore.get('cookiesvalue')) {
          $state.go('app.all-users');
        }else {
          $scope.user = {
            email: 'rohit.keshri@indusnet.co.in',
            password: '12345'
          };

          $scope.login = function() {
            AuthService.adminlogin($scope.user.email, $scope.user.password)
              .then(function(data) {
                 if(data==false){
                   $scope.error="Wrong Email or Password";
                   $scope.erroricon="fa fa-warning text-danger";
                   return false;
                 }else{
                  var now = new Date();
                  var timeout=$rootScope.currentUser.timeout > 0 ? $rootScope.currentUser.timeout:20;
                  var exp = new Date(now.getTime() + timeout*1000);
                  $cookieStore.put('cookiesvalue',$rootScope.currentUser,{'expires': exp});
                  $state.go('app.all-users');
                }

              });
          };
         $scope.resetpassword=function(){
            AuthService.resetpassword($scope.guest.email)
              .then(function(data) {
                if(data==false){
                  $scope.errorForpass="The email you provide is invalid.";
                  $scope.errorPassicon="fa fa-warning text-danger";
                  return false;
                }else{
                  $scope.errorForpass="Check your email.";
                  $scope.errorPassicon="fa fa-thumbs-o-up text-success";
                  return false;
                }
              });
          };
        }

  }])
  .controller('AuthLogoutController', ['$scope', 'AuthService', '$state','$cookieStore','$rootScope',
      function($scope, AuthService, $state,$cookieStore,$rootScope) {
      var currentUser=$cookieStore.get('cookiesvalue');
      if(currentUser){
        var redirect=currentUser.realm=='Superadmin'?'adminlogin':'login';
        //  AuthService.logout()
        //    .then(function() {

              $cookieStore.remove('cookiesvalue');
              $state.go(redirect);
          //  });
      }else{
        $state.go('login');
      }

  }])
  .controller('SignUpController', ['$scope', 'AuthService', '$state',
      function($scope, AuthService, $state) {
    $scope.user = {
      email: 'baz@qux.com',
      password: 'bazqux'
    };

    $scope.register = function() {
      alert($scope.user.name);
      AuthService.register($scope.user.name, $scope.user.username,$scope.user.email,$scope.user.description,$scope.user.password)
        .then(function() {
          $state.transitionTo('sign-up-success');
        });
    };
  }]);
