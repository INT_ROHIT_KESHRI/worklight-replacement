var foo = (function(res, successCallBack, errorCallBack){
    var response = {
        "status" : res.body
    };
    if(res.statusCode==200){
      successCallBack(response);
    }else{
      errorCallBack(response);
    }


});
module.exports=foo;