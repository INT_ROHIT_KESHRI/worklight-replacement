var loopback = require('loopback');
var boot = require('loopback-boot');
var app = module.exports = loopback();
var bodyParser = require('body-parser');
var fs = require('fs');
var CronJob = require('cron').CronJob;
var dataSource = require('../Lib/config/datasource.js');
var jade = require('jade');
var path = require('path');
/*** Middleware to add bodyparser ***/
app.use(bodyParser.urlencoded({
    extended: true
}));
//app.use(loopback.token({ model: app.models.accessToken, currentUserLiteral: 'me' }));
app.use(loopback.token({ model: app.models.accessToken }));
app.set('views', path.join(__dirname, 'boot/views'));
app.set('view engine', 'jade');
/*** END ***/
var path = require('path');
var mime = require('mime');
app.get('/download', function(req, res){
  console.log("Download");
  //Here do whatever you need to get your file
  var file="client/pdf/Admin_panel_guide.pdf";
  var filename = path.basename(file);
  var mimetype = mime.lookup(file);
  res.setHeader('Content-disposition', 'attachment; filename=' + filename);
  res.setHeader('Content-type', mimetype);
  var filestream = fs.createReadStream(file);
  filestream.pipe(res);
});

/*** Cron Script to send reminder mail to the users whose account disable after 5 days ***/

var job = new CronJob('00 30 7 * * 0-6', function() {
  /*
   * Runs every day (Monday through Sunday)
   * at 7:30:00 AM. It does not run on Saturday
   * or Sunday.
   */
     var Client=app.loopback.findModel('client');
     Client.attachTo(dataSource.Datasource);
     app.model(Client);
     var Model=app.loopback.findModel('model');
     Model.attachTo(dataSource.Datasource);
     app.model(Model);
     var A=function(callback){
       var Six_days = 6 * 24 * 60 * 60 * 1000;  // Six days in milliseconds
        Client.find({where:{realm:{neq:'Superadmin'},status:1,validity_to:{lt: Date.now()+Six_days}}},function(err,client){
          client.forEach(function(fieldname, index) {
            app.models.Mail.sendEmail({body:{id:fieldname.id,type:'reminder'}},function(err, items){
            });
          });
          callback();
        });
     };
     var B=function(){
         var one_day = 1 * 24 * 60 * 60 * 1000;  // Six days in milliseconds
         Client.find({where:{realm:{neq:'Superadmin'},status:1,validity_to:{lt: Date.now()-one_day}},include:['model']},function(err,client){
           var blank = [];
            client.forEach(function(field,index){
             var model_value=client[index]['model']();
             app.models.Mail.sendEmail({body:{id:field.id,type:'disable'}},function(err, items){
              });
              if(JSON.stringify(blank)!=model_value){
                model_value.forEach(function(fieldname,index){
                  Model.updateAll({id:fieldname.id},{statuschange:1,flag:0})
                });
              }
           });
          Client.updateAll({status:1,realm:{neq:'Superadmin'},validity_to:{lt: Date.now()-one_day}}, {status: 0,realm:''});
         });
     };
     A(function(){
       B();
     });
  }, null, true, /* Start the job right now */ 'America/Los_Angeles' /* Time zone of this job. */);
/*** End ***/


app.use(loopback.token({ model: app.models.accessToken, currentUserLiteral: 'me' }));
app.start = function() {
  // start the web server
  return app.listen(function() {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module)
    app.start();
});
