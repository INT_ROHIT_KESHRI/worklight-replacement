var loopback = require('loopback');
var app = module.exports = loopback();
var DataSource = require('loopback-datasource-juggler').DataSource;
var dataSource = new DataSource({
    connector: require('loopback-connector-mysql'),
    host: 'localhost',
    port: 3306,
    database: 'kwallet2_angular',
	username:'root',
	password:''
});
var properties=require('../../Lib/config/properties.js');
app.model('tokensecret',{dataSource: dataSource});
app.model('api',{dataSource: dataSource,properties: properties.Apis});
var request = require("request");
var uuid = require('node-uuid');
var datetime = require('node-datetime');
module.exports = function() {
	return function(req, res, next) {
	var API = app.models.api;	
	API.findOne({where:{url:req.url}},function(err,api){
		console.log(api);
		if(api==null){
			next();
		}else{
			var token = (req.body && req.body.access_token) || (req.query && req.query.access_token) || req.headers['x-access-token'];
			var LBinstanceId = (req.body && req.body.lbinstance_id) || req.headers['lbinstance_id'];
			var AscessToken = app.models.tokensecret;
			if (token) {
				try {
					var dt = datetime.create(new Date());
					var created = dt.format("Y-m-d H:M:S");
					AscessToken.find({
						where: {
							lbinstance_id: LBinstanceId,
							tokensecret: token
						}
					}, function(err, secret) {
						if (secret.length > 0) {
							next();
						} else {
							res.end('Invalid Ascess Token', 400);
						}
					});
				} catch (err) {
					throw err
				}
			} else {
				next();
			}
		}	
    });
}
};