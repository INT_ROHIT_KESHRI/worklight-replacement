function identifier(){
		 var returnedObject = {};
		 var loopback= require('loopback');
		 returnedObject["app"] = module.exports = loopback();
		 returnedObject["properties"] = require('../../Lib/config/properties.js');
		 returnedObject["dataSource"] = require('../../Lib/config/datasource.js');
		 return returnedObject;
		 }
function getapi(url,cb){
	var identifiers=identifier();
		identifiers.app.model('ratelimiter',{dataSource: identifiers.dataSource.Datasource,properties: identifiers.properties.Ratelimiter});
		identifiers.app.model('apis',{dataSource: identifiers.dataSource.Datasource,properties: identifiers.properties.Apis});
		var Ratelimiter = identifiers.app.models.ratelimiter;
		var Apis = identifiers.app.models.apis;
		Apis.find({where:{api:url,ratelimiter_id:{nqe:0}} }, function(err, data) {
			if(data.length>0){
			Ratelimiter.find({where:{id:data[0].ratelimiter_id}},function(error,result){
					if(result.length > 0){
						cb(result);
					}else{
						cb(false);
					}
			});
			}else{
				cb(false);
			}

		  });
}
exports.LBratelimiter = getapi;
