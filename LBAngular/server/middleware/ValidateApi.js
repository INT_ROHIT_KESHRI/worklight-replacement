function identifier(){
		 var returnedObject = {};
		 var loopback= require('loopback');
		 returnedObject["app"] = module.exports = loopback();
		 returnedObject["properties"] = require('../../Lib/config/properties.js');
		 returnedObject["dataSource"] = require('../../Lib/config/datasource.js');
		 return returnedObject;
		 }

function getApi(cb){
	var identifiers=identifier();
		identifiers.app.model('apis',{dataSource: identifiers.dataSource.Datasource,properties: identifiers.properties.Apis});
		var Apis = identifiers.app.models.apis;	 
		Apis.find({
			fields: {api: true},
            where: {
                flag: 1
            }
        }, function(err,apis) {
            if (apis.length == 0) {
                cb(null);
            } else {
				var iplist=[];
                var x;
				for (x=0;x<apis.length;x++) {
					iplist[x]=apis[x]['api'];
				}
				cb(iplist);
            }
        });
}
module.exports = function() {
return function(req, res, next) {
		getApi(function(result){
		var ValidApi=result;
		var test=ValidApi.indexOf(req.url);
		if(test==-1){
			res.json(404,"Api Not Found");
		}else{
			next();
		}
		});
		
};
};