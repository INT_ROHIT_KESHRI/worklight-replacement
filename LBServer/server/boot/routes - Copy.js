var jwt = require('jwt-simple');
var request = require("request");
var uuid = require('node-uuid');
var datetime = require('node-datetime');

module.exports=function(app){
		app.post('/api/kwallet/login', function(req, res) {
		request({
			uri: "http://10.0.8.213/api/rest/login",
			method: "POST",
			form: {email: req.body.email,password:req.body.password}
		}, function(error, response, body) {
			//console.log(body);
			if(response.statusCode==200){
				var token = (req.body && req.body.access_token) || (req.query && req.query.access_token) || req.headers['x-access-token'];
				var LBinstanceId = (req.body && req.body.lbinstance_id) || req.headers['lbinstance_id'];
				if(!token){
					var result=JSON.parse(body);
					var dt = datetime.create(new Date());
					var created=dt.format("Y-m-d H:M:S")
					var ret = new Date();
					var expire=ret.setTime(ret.getTime() + 2*60000);
					var token = jwt.encode({iss: result.id,exp: expire},'jwtTokenSecret');
					app.models.tokensecret.find({ where: {lbinstance_id: LBinstanceId}}, function(err,secret) {
					   if(err)console.log(err);	
					  if (secret) { // if the record exists in the db
						app.models.tokensecret.updateAll({lbinstance_id: LBinstanceId},{tokensecret:token,expiretime:expire,updated:created},function(err,info){
							if(err) throw err;
							res.json({token : token});
						});
					 }else{
						  res.json({"error":"Invalid LBInstance ID"});
					  }
					});
				}else{
					console.log("final");
				    res.send(body);
				}	
			}else{
				res.json({"error":"Invalid Credetials"});
			}
		}); 
  });
  app.get('/api/Kwallet/getLBInstanceId',function(req,res){
	  console.log("ENter");
	  var LBinstanceId = uuid.v1();
	  var now = new Date();
	  var dt = datetime.create(now);
      var created=dt.format("Y-m-d H:M:S");;
	  app.models.tokensecret.create([
	  {lbinstance_id:LBinstanceId,created:created,modified:created}
	  ],function(err){
		  if(err) throw err;
	  });
	  res.json({
		  LBinstanceId:LBinstanceId,
		  aaaaaaaa:"sssssss"
	  });
  });
};