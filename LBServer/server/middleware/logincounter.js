var loopback = require('loopback');
var app = module.exports = loopback();
var DataSource = require('loopback-datasource-juggler').DataSource;
var Statuscode = require('../../Lib/statuscode.js');
var uuid = require('node-uuid');
var dataSource = new DataSource({
    connector: require('loopback-connector-mysql'),
    host: 'localhost',
    port: 3306,
    database: 'kwallet2_angular',
    username: 'root',
    password: ''
});
app.model('logincounter', {
    dataSource: dataSource
});
app.model('auth', {
    dataSource: dataSource
});
var Logincounter = app.models.logincounter;
var Auth = app.models.auth;
module.exports = function() {
    return function(req, res, next) {
      if (req.set_api_details.details.api_fields != '' && req.set_api_details.details.method == 'POST' && req.cookies) {
        Auth.find({
          where: {
            api_id: req.set_api_details.details.api_id,
            flag: 1,
            restrict_login:1,
          }
        }, function(err, log) {
          if (log.length > 0) {
            var Interval = log[0].attempt_interval * 60 * 1000;
            Logincounter.find({
              where:{
                cookie:req.cookies.cookiename,
                counter:{gt:log[0].max_attempts},
                updated:{gt: Date.now() - Interval}
              }
            },function(error,counter){
              if(counter.length>0){
                  res.json(Statuscode.Status[416]);
              }else{
                next();
              }
            })
          } else {
            next();
          }
        });
      }else{
        next();
      }
    }
};
