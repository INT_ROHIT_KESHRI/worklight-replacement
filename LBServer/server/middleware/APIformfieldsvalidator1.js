var validation = require('../../Lib/validation.js');
var Statuscode = require('../../Lib/statuscode.js');
module.exports = function() {
    return function(req, res, next) {
        var fields_value = {};
        var fields = JSON.parse(req.set_api_details.details.api_fields);
        var allfields = Object.keys(fields).map(function(k) {
            return fields[k]
        });
        if (Object.keys(req.body).length > 0) {
            var reqbody = req.body;
            if (Object.keys(reqbody).length > allfields.length) {
                res.json(Statuscode.Status['408']);
                return false;
            }
            var fieldname;
            var keys = [];
            var types = [];
            var required = [];
            var param_keys = [];
            var param_values = [];
            var type;

            var A = function(callback) {
              setTimeout(function() {
                allfields.forEach(function(fieldname, index) {
                    fieldname = fieldname.split("!!");
                    type = fieldname[1].split("|");
                    keys[index] = fieldname[0];
                    types[index] = type[0];
                    required[index]=(type[1]=='Required')?true:false;
                });
                callback();
              }, 100);
            };
            var B = function(callback2) {
              setTimeout(function(){
                var allobjectkeys=Object.keys(reqbody);
                for (var i = 0; i < allobjectkeys.length; i++) {
                      if(keys.indexOf(allobjectkeys[i])==-1){
                        console.log("Index :"+keys.indexOf(allobjectkeys[i]));
                        res.json(Statuscode.Status['408']);
                        return false;
                      }else{
                        param_keys[i]=allobjectkeys[i];
                        param_values[i]=reqbody[allobjectkeys[i]];
                      }
                      if(i==allobjectkeys.length-1){
                        callback2();
                      }
                  }

              },100);
            };
            var C=function(callback3){
              setTimeout(function(){
                console.log("Required:::::"+required);
                required.forEach(function(require, index) {
                  setTimeout(function(){
                    var param_index=param_keys.indexOf(keys[index]);
                    if(require==true && param_index==-1){
                          console.log("Error");
                          Statuscode.Status[411].parameter=keys[index];
                          res.json(Statuscode.Status[411]);
                          delete Statuscode.Status[411].parameter;
                        return false;
                    }else{

                      var paramstype = validation.Validate.formfields(types[param_index], param_values[param_index]);
                      if(paramstype==false){
                        console.log("Error1");
                        Statuscode.Status[412].parameter=keys[index];
                        res.json(Statuscode.Status[412]);
                        delete Statuscode.Status[411].parameter;
                        return false;
                      }else{
                        fields_value[keys[index]]=reqbody[keys[index]];
                      }
                    }
                  },200);
                });
                callback3();
              },100);
            };
            var D=function(){
              setTimeout(function(){
                var require_field_len=required.filter(function(value){
                  return value === true;
                }).length;
                if (Object.keys(fields_value).length>=require_field_len) {
                    req.post_fields = {
                        details: fields_value
                    };
                    next();
                }
              });
            };
            A(function() {
              B(function(){
                C(function(){
                  D();
                });
              });
            });
        } else {
           if(allfields.length < 1){
             req.post_fields = {
                 details: ''
             };
             next();
           }else{
             res.json(Statuscode.Status['411']);
             return false;
           }
        }
    };
};
