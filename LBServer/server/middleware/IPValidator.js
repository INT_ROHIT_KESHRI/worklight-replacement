function identifier() {
    var returnedObject = {};
    var loopback = require('loopback');
    returnedObject["app"] = module.exports = loopback();
    returnedObject["properties"] = require('../../Lib/config/properties.js');
    returnedObject["dataSource"] = require('../../Lib/config/datasource.js');
    return returnedObject;
}

function getIp(cb) {
    var identifiers = identifier();
    identifiers.app.model('ipaddress', {
        dataSource: identifiers.dataSource.Datasource,
        properties: identifiers.properties.Ipaddress
    });
    var Ipaddress = identifiers.app.models.ipaddress;
    Ipaddress.find({
        fields: {
            ipaddress: true
        },
        where: {
            flag: 1
        }
    }, function(err, ips) {
        if (ips.length == 0) {
            cb(null);
        } else {
            var iplist = [];
            var x;
            for (x = 0; x < ips.length; x++) {
                iplist[x] = ips[x]['ipaddress'];
            }
            cb(iplist);
        }
    });
}

exports.LBipvalidater = getIp;
