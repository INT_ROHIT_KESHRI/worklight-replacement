function identifier() {
    var returnedObject = {};
    var loopback = require('loopback');
    returnedObject["app"] = module.exports = loopback();
    returnedObject["properties"] = require('../../Lib/config/properties.js');
    returnedObject["dataSource"] = require('../../Lib/config/datasource.js');
    return returnedObject;
}

function Checkresponse(api_id, time) {
    var identifiers = identifier();
    identifiers.app.model('responsetime', {
        dataSource: identifiers.dataSource.Datasource,
        properties: identifiers.properties.Responsetime
    });
    var Responsetime = identifiers.app.models.responsetime;
    var status = 'fast';
    Responsetime.find({
        where: {
            api_id: api_id,
            flag: {
                neq: 0
            }
        }
    }, function(err, data) {
        if (data.length > 0) {
            if (time > data[0].time) status = 'slow';
            Responsetime.updateAll({
                api_id: api_id
            }, {
                status: status
            }, function(err, info) {
                if (err) throw err;
            });
        }
    });
    return
}
exports.Responsetime = Checkresponse;
