var validation = require('../../Lib/validation.js');

function identifier() {
    var returnedObject = {};
    var loopback = require('loopback');
    returnedObject["app"] = module.exports = loopback();
    returnedObject["properties"] = require('../../Lib/config/properties.js');
    returnedObject["dataSource"] = require('../../Lib/config/datasource.js');
    return returnedObject;
}

function getApi(req, cb) {
    var identifiers = identifier();
    identifiers.app.model('api', {
        dataSource: identifiers.dataSource.Datasource,
        properties: identifiers.properties.Apis
    });
    var Api = identifiers.app.models.api;
    var requrl = req.url.split("?");
    var actual_url = requrl[0];
    Api.find({
        fields: {
            url: true,
            id: true,
            model_id: true,
            method: true,
            uri: true,
            name: true,
            fields: true,
            token: true
        },
        where: {
            flag: 1,
            url: actual_url
        }
    }, function(err, apis) {
        if (apis.length == 0) {
            cb(null);
        } else {
            if (req.method != apis[0].method) {
                cb("Wrongmethod");
                return false;
            }
            var fields = JSON.parse(apis[0].fields);
            var allfields = Object.keys(fields).map(function(k) {
                return fields[k]
            });
            var fieldname;
            var keys = [];
            var types = [];
            var required=[];
            var type;
            allfields.forEach(function(fieldname, index) {
                fieldname = fieldname.split("!!");
                type = fieldname[1].split("|");
                keys[index] = fieldname[0];
                types[index] = type[0];
                required[index]=(type[1]=='Required')?true:false;
            });
            console.log("Required"+required);
            console.log("Keys"+keys);
            console.log("Types"+types);
            if (req.method == 'GET') {
                if (requrl[1]) {
                    var params = requrl[1].split("&");
                    var i;
                    var params_length = params.length;
                    for (i = 0; i < params_length; i++) {
                        var variable = params[i].split("=");
                        console.log("Type"+i+":"+types[i]);
                        console.log("Variable"+i+":"+variable[1]);
                        console.log("Required"+i+":"+required[i]);
                        var paramstype = validation.Validate.formfields(types[i], variable[1]);
                        if (keys.indexOf(variable[0]) == -1 || paramstype == false) {
                            cb("Wrongparams");
                            return false;
                        }
                    }
                    params = "?" + requrl[1];
                } else {
                    var params = '';
                }
            }
            return false;
            var url = apis[0].url.split("/");
            var api_details = {
                model_id: apis[0].model_id,
                api_id: apis[0].id,
                url: apis[0].url,
                api_uri: apis[0].uri,
                api_name: apis[0].name,
                method: apis[0].method,
                model_name: url[2],
                api_params: params,
                api_fields: apis[0].fields,
                api_token_status: apis[0].token
            };
            cb(api_details);
        }
    });
}
module.exports = function() {
    return function(req, res, next) {
        getApi(req, function(response) {
            if (response == null) {
                res.json(404, "Api Not Found");
            } else if (response == "Wrongparams") {
                res.json(400, "Wrong params");
            } else if (response == "Wrongmethod") {
                res.json(400, "Wrong Method");
            } else {
                req.set_api_details = {
                    details: response
                };
                next();
            }
        });

    };
};
