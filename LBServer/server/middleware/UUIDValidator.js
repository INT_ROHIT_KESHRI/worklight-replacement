function identifier() {
    var returnedObject = {};
    var loopback = require('loopback');
    returnedObject["app"] = module.exports = loopback();
    returnedObject["properties"] = require('../../Lib/config/properties.js');
    returnedObject["dataSource"] = require('../../Lib/config/datasource.js');
    return returnedObject;
}

module.exports = function() {
    return function(req, res, next) {
        var Statuscode = require('../../Lib/statuscode.js');
        var identifiers = identifier();
        identifiers.app.model('model', {
            dataSource: identifiers.dataSource.Datasource,
            properties: identifiers.properties.Models
        });
        var Model = identifiers.app.models.model;
        var UUID = (req.body && req.body.lbinstance_id) || req.headers['uuid'];
        if (!UUID) res.json(Statuscode.Status[418]);
        var Model_name = req.set_api_details.details.model_name;
        Model.find({
            where: {
                flag: 1,
                name: Model_name,
                UUID: UUID
            }
        }, function(err, model) {
            if (model[0]) {
                next();
            } else {
                res.json(Statuscode.Status[415]);
            }
        });

    };
};
