var loopback = require('loopback');
var app = module.exports = loopback();
var DataSource = require('loopback-datasource-juggler').DataSource;
var Statuscode = require('../../Lib/statuscode.js');
var dataSource = new DataSource({
    connector: require('loopback-connector-mysql'),
    host: 'localhost',
    port: 3306,
    database: 'kwallet2_angular',
    username: 'root',
    password: ''
});
var properties = require('../../Lib/config/properties.js');
app.model('tokensecret', {
    dataSource: dataSource,
    properties:properties.Tokensecret
});
var request = require("request");
var uuid = require('node-uuid');
var datetime = require('node-datetime');
module.exports = function() {
    return function(req, res, next) {
        var token = (req.body && req.body.access_token) || (req.query && req.query.access_token) || req.headers['x-access-token'];
        var LBinstanceId = (req.body && req.body.lbinstance_id) || req.headers['lbinstance_id'];
        var AscessToken = app.models.tokensecret;
        if (req.set_api_details.details.is_logout_api==1) {
            if(token){
                AscessToken.updateAll({
                    lbinstance_id: LBinstanceId,
                    tokensecret:token
                }, {
                    tokensecret: '',
                    user_id: 0
                }, function(err, info) {
                  Statuscode.Status[200].data="Your are successfully logout";
                  res.json(Statuscode.Status[200]);
                  delete Statuscode.Status[200].data;
                });

            }else{
              res.json(Statuscode.Status['401']);
            }
        } else {
            next();
        }
    }
};
