var loopback = require('loopback');
var app = module.exports = loopback();
var properties = require('../../Lib/config/properties.js');
var dataSource = require('../../Lib/config/datasource.js');
var Statuscode = require('../../Lib/statuscode.js');
app.model('tokensecret', {
    dataSource: dataSource.Datasource,
    properties: properties.Tokensecret
});
var request = require("request");
var uuid = require('node-uuid');
var datetime = require('node-datetime');

module.exports = function() {
    return function(req, res, next) {
        var token = (req.body && req.body.access_token) || (req.query && req.query.access_token) || req.headers['x-access-token'];
        var LBinstanceId = (req.body && req.body.lbinstance_id) || req.headers['lbinstance_id'];
        var AscessToken = app.models.tokensecret;
        var dt = datetime.create(new Date());
        var created = dt.format("Y-m-d H:M:S");
        var ret = new Date();
        var currentime = ret.getTime();
        var expire = ret.setTime(ret.getTime() + 10 * 60000);
        if (!LBinstanceId) {
            try {
                var LBinstanceId = uuid.v1();
                if (token) {
                    AscessToken.find({
                        where: {
                            tokensecret: token
                        }
                    }, function(err, tokensecret) {
                        if (tokensecret.length == 0) {
                            //res.end('Invalid Ascess Token', 400);
                            res.json(Statuscode.Status[416]);
                        } else {
                            AscessToken.updateAll({
                                id: tokensecret[0]['id']
                            }, {
                                lbinstance_id: LBinstanceId,
                                expiretime: expire,
                                updated: created
                            }, function(err, info) {
                                if (err) throw err;
                                Statuscode.Status[200].lbinstance_id=LBinstanceId;
                                res.json(Statuscode.Status[200]);
                                delete Statuscode.Status[200].lbinstance_id;
                                /*res.json({
                                    "lbinstance_id": LBinstanceId
                                });*/
                            });
                        }
                    });
                } else {
                    AscessToken.create([{
                        lbinstance_id: LBinstanceId,
                        created: created,
                        updated: created,
                        expiretime: expire
                    }], function(err, data) {
                        if (err) throw err;
                        Statuscode.Status[200].lbinstance_id=LBinstanceId;
                        res.json(Statuscode.Status[200]);
                        delete Statuscode.Status[200].lbinstance_id;
                      /*  res.json({
                            "lbinstance_id": LBinstanceId
                        });*/
                    });
                }
            } catch (err) {
                throw err;
            }
        } else {
            AscessToken.find({
                where: {
                    lbinstance_id: LBinstanceId
                }
            }, function(err, tokensecret) {
                // console.log(tokensecret);
                if (tokensecret.length == 0) {
                    //res.end('Invalid LBinstance Id', 400);
                    res.json(Statuscode.Status[417]);
                } else {
                    var LBinstanceId = uuid.v1();
                    var expire_time = new Date(tokensecret[0]['expiretime']).getTime();
                    if (currentime > expire_time) {
                        AscessToken.updateAll({
                            id: tokensecret[0]['id']
                        }, {
                            lbinstance_id: LBinstanceId,
                            expiretime: expire,
                            updated: created
                        }, function(err, info) {
                            if (err) throw err;
                            Statuscode.Status[200].lbinstance_id=LBinstanceId;
                            res.json(Statuscode.Status[200]);
                            delete Statuscode.Status[200].lbinstance_id;
                            /* res.json({
                                "lbinstance_id": LBinstanceId
                            });*/
                        });
                    } else {
                        AscessToken.updateAll({
                            id: tokensecret[0]['id']
                        }, {
                            expiretime: expire,
                            updated: created
                        }, function(err, info) {
                            if (err) throw err;
                            else next();
                        });
                    }
                }
            });
        }
    }
};
