var loopback = require('loopback');
var app = module.exports = loopback();
var dataSource = require('../../Lib/config/datasource.js');
var properties = require('../../Lib/config/properties.js');
module.exports = function() {
	return function(req, res, next) {
		app.model('ratelimiter', {
				dataSource: dataSource.Datasource,
				properties: properties.Ratelimiter
		});
	  var RATELIMITER = app.models.ratelimiter;
		var API_ID=req.set_api_details.details.api_id;
		var API_URL=req.set_api_details.details.url;
		RATELIMITER.find({where:{api_id:API_ID,flag:1}},function(err,ratelimiter){
			if(ratelimiter[0]){

				var RateLimit = require('express-rate-limit');
				/*var apiLimiter = new RateLimit({
					windowMs: ratelimiter[0].duration, // set time to block the api after this time
					max: ratelimiter[0].hits,          // after this number of hits the api block
					delayMs: 0 // disabled
				});*/
				var apiLimiter = new RateLimit({
					windowMs: 900000, // set time to block the api after this time
					max: 5,          // after this number of hits the api block
					delayMs: 100000 // disabled
				});
				app.use(apiLimiter);
				next();
				/*
				var ret = new Date();
				var currentime = ret.getTime();
				var expire_time = new Date(ratelimiter[0]['duration']).getTime();
				console.log("CurrentTime"+currentime);
				console.log("ExpireTimee"+expire_time);
				if(ratelimiter[0].remaininghits>0 && expire_time>currentime){
						RATELIMITER.updateAll({api_id:API_ID},{remaininghits: ratelimiter[0].remaininghits-1}, function(err, info) {
							console.log("Error"+err);
							console.log("Info"+info);
						});
						next();
				}else{
					res.send("You have exceed the number of hits or the time expires.Contact the administrator");
				}*/
			}else {
				next();
			}
		});
	};
};
