function identifier() {
    var returnedObject = {};
    var loopback = require('loopback');
    returnedObject["app"] = module.exports = loopback();
    returnedObject["properties"] = require('../../Lib/config/properties.js');
    returnedObject["dataSource"] = require('../../Lib/config/datasource.js');
    return returnedObject;
}

function ratelimiter(api_id, cb) {
    var identifiers = identifier();
    identifiers.app.model('ratelimiter', {
        dataSource: identifiers.dataSource.Datasource,
        properties: identifiers.properties.Ratelimiter
    });
    var RATELIMITER = identifiers.app.models.ratelimiter;
    RATELIMITER.find({
        where: {
            api_id: api_id,
            flag: 1
        }
    }, function(error, result) {
        console.log(JSON.stringify(result));
        if (result.length > 0) {
            cb(result);
        } else {
            cb(false);
        }
    });
}
exports.LBratelimiter = ratelimiter;
