var validation = require('../../Lib/validation.js');
var Statuscode = require('../../Lib/statuscode.js');
function identifier() {
    var returnedObject = {};
    var loopback = require('loopback');
    returnedObject["app"] = module.exports = loopback();
    returnedObject["properties"] = require('../../Lib/config/properties.js');
    returnedObject["dataSource"] = require('../../Lib/config/datasource.js');
    return returnedObject;
}

function getApi(req, cb) {
    var identifiers = identifier();
    identifiers.app.model('api', {
        dataSource: identifiers.dataSource.Datasource,
        properties: identifiers.properties.Apis
    });
    identifiers.app.model('model', {
        dataSource: identifiers.dataSource.Datasource,
        properties: identifiers.properties.Models
    });
    var Api = identifiers.app.models.api;
    var Model = identifiers.app.models.model;
    var requrl = req.url.split("?");
    var actual_url = requrl[0];
    Api.find({
        fields: {
            url: true,
            id: true,
            model_id: true,
            method: true,
            uri: true,
            name: true,
            fields: true,
            token: true,
            is_logout:true
        },
        where: {
            flag: 1,
            url: actual_url
        }
    }, function(err, apis) {
        if (apis.length == 0) {
            cb({'responsecode':413,'value':''});
        } else {
            if (req.method != apis[0].method) {
                cb({'responsecode':406,'value':''});
                return false;
            }
            Model.findById(apis[0].model_id,function(err,models){
              var url = apis[0].url.split("/");
              var api_details = {
                  model_id: apis[0].model_id,
                  api_id: apis[0].id,
                  url: apis[0].url,
                  api_uri: apis[0].uri,
                  api_name: apis[0].name,
                  method: apis[0].method,
                  model_name: url[2],
                  api_fields: apis[0].fields,
                  api_token_status: apis[0].token,
                  tracking_id:models.tracking_id,
                  is_logout_api:apis[0].is_logout
              };
            if (req.method == 'GET'|| req.method == 'DELETE') {
                if (requrl[1]) {
                  var fields = JSON.parse(apis[0].fields);
                  var allfields = Object.keys(fields).map(function(k) {
                      return fields[k]
                  });
                  var fieldname;
                  var keys = [];
                  var types = [];
                  var required=[];
                  var type;
                  var param_keys=[];
                  var param_values=[];
                  allfields.forEach(function(fieldname, index) {
                      fieldname = fieldname.split("!!");
                      type = fieldname[1].split("|");
                      keys[index] = fieldname[0];
                      types[index] = type[0];
                      required[index]=(type[1]=='Required')?true:false;
                  });
                  var params = requrl[1].split("&");
                  var i;
                  var params_length = params.length;
                  if(params_length>required.length){
                    cb({'responsecode':408,'value':''});
                    return false;
                  }
                  for (i = 0; i < params_length; i++) {
                      var variable = params[i].split("=");
                      if(keys.indexOf(variable[0])==-1){
                        cb({'responsecode':408,'value':variable[0]});
                        return false;
                      }else{
                        param_keys[i]=variable[0];
                        param_values[i]=variable[1];
                      }
                  }
                  var generated_url='';
                  required.forEach(function(require, index) {
                    var param_index=param_keys.indexOf(keys[index]);
                    if(require==true && param_index==-1){
                        cb({'responsecode':411,'value':keys[index]});
                        return false;
                    }else{
                      var paramstype = validation.Validate.formfields(types[param_index], param_values[param_index]);
                      if(paramstype==false){
                        cb({'responsecode':412,'value':keys[index]});
                        return false;
                      }else{
                        generated_url=generated_url+keys[index]+"="+param_values[param_index]+"&";
                      }
                    }
                  });
                  api_details.api_params="?" + generated_url.slice(0,-1);
                  cb({'responsecode':200,'value':api_details});
                } else {
                  api_details.api_params='';
                  cb({'responsecode':200,'value':api_details});
                }
            }else{
                  api_details.api_params='';
                  cb({'responsecode':200,'value':api_details});
            }
          });
        }
    });
}
module.exports = function() {
    return function(req, res, next) {
        getApi(req, function(response) {
           var code=response.responsecode;
           var value=response.value;
            if (code == 200) {
                req.set_api_details = {
                    details: value
                };
                next();
            } else  {
                if(value!=''){Statuscode.Status[code].parameter=value;}
                res.json(Statuscode.Status[code]);
                delete Statuscode.Status[code].parameter;
              //  Statuscode.Status[code][code]=Statuscode.Status[code][code]+" "+value;
              //  res.json(Statuscode.Status[code]);
            }
        });

    };
};
