var loopback = require('loopback');
var boot = require('loopback-boot');
var bodyParser = require('body-parser');
var app = module.exports = loopback();
var nodalytics = require('nodalytics');
var CronJob = require('cron').CronJob;
var ipfilter = require('express-ipfilter');
var ipvalidater=require('./middleware/IPValidator');
var Responsetime=require('./middleware/LBresponsetime');
var ratelimiter=require('./middleware/LBratelimiter');
//var test=require('./middleware/test');

/*app.defineMiddlewarePhases([
  'initial', 'postinit', 'preauth', 'routes', 'subapps'
]);*/

/*** Middleware to add bodyparser ***/
app.use(bodyParser.urlencoded({
    extended: true
}));
/*** End ***/
/*** Middleware to enable only selected IP address ***/
ipvalidater.LBipvalidater(function(returnedIps){app.middleware('initial:before', ipfilter(returnedIps, {mode: 'allow'}));});
/*** End ***/

/***   Rate limiter  ***/
app.middleware('initial:after',['/'],function(req,res,next){
	var API_ID=req.set_api_details.details.api_id;
	var API_URL=req.set_api_details.details.url;
	var RateLimit = require('express-rate-limit');
	ratelimiter.LBratelimiter(API_ID,function(details){
		if(details!=false){
      console.log("details "+JSON.stringify(details));
			var apiLimiter = new RateLimit({
				windowMs: details[0].duration, // set time to block the api after this time
				max: details[0].hits,  // after this number of hits the api block
				delayAfter: details[0].hits-2,
				delayMs: 0,
				message: "Too many hits from this IP, please try again after "+(details[0].duration/60000)+" mins"
			});
			app.use(API_URL, apiLimiter);
		}
	});
	next();
});
/*** End ***/

/***Track the  Response time of a Api***/
var responseTime = require('response-time');
app.middleware('routes',responseTime(function (req, res, time) {
	Responsetime.Responsetime(req.set_api_details.details.api_id,time);
  console.log("Time :"+time);
}));
/*** End ***/

/*** Google Anlaytics Middleware if the previous Middleware Qualifies ***/
app.use(loopback.cookieParser('your secret here'));
app.use(loopback.session());
/*app.middleware('routes',['/'],nodalytics({
    property_id:'',
    map: function (req, event) {
      console.log(req.sessionID);
      //event.ea="Login";
      event.tid=(req.set_api_details.details.tracking_id=='')?'UA-75858027-1':req.set_api_details.details.tracking_id;;
    //  event.el="Success";
    //  event.ev=1;
			console.log("Event"+JSON.stringify(event,null,2));
    //  console.log("Tracking Id :"+req.custom_status);
		//	return event;
    },
    error: function (error, event, headers, req, grec, gres) {
    },
    success: function (event, headers, req, grec, gres) {
    }
}));*/
//app.middleware('subapps', test());
/*function errorhandler() {
//  console.log("Tracking Id :"+req.custom_status);
  //next();
}*/
//app.use(require('./middleware/test')());
/*** End ***/

/*** Cron Script to delete the invalid InstanceId from database on evry Friday 11.30 pm ***/
new CronJob('00 30 23 * * 7', function() {
	var pastdate = new Date();
    pastdate.setDate(pastdate.getDate() - 3);
	  app.models.tokensecret.destroyAll({ updated: {lt: pastdate} }, function(req,res){
	})
}, null, true, 'America/Los_Angeles');
/*** End ***/

app.start = function() {
    // start the web server
    return app.listen(function() {
        app.emit('started');
        var baseUrl = app.get('url').replace(/\/$/, '');
        console.log('Web server listening at: %s', baseUrl);
        if (app.get('loopback-component-explorer')) {
            var explorerPath = app.get('loopback-component-explorer').mountPath;
            console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
        }
    });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
    if (err) throw err;

    // start the server if `$ node server.js`
    if (require.main === module){
		app.start();
	}

});
