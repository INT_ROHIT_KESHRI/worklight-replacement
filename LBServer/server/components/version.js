module.exports = function (loopbackApplication) {
  var version = loopbackApplication.loopback.version;
  console.log('LoopBack v%s', version);
};