var status_code={
        200:{
          "Statuscode":200,
          "Statustext":"Success"
        },
        400:{
          "Statuscode":400,
          "Statustext":"Bad Request"
        },
        401:{
          "Statuscode":401,
          "Statustext":"Unauthorized"
        },
        403:{
          "Statuscode":403,
          "Statustext":"Forbidden"
        },
        404:{
          "Statuscode":404,
          "Statustext":"Not Found"
        },
        406:{
          "Statuscode":406,
          "Statustext":"Given method is wrong"
        },
        408:{
          "Statuscode":408,
          "Statustext":"Please remove unwanted parameters"
        },
        411:{
          "Statuscode":411,
          "Statustext":"Please give required parameter"
        },
        412:{
          "Statuscode":412,
          "Statustext":"Please supply correct datatype for the parameter"
        },
        413:{
          "Statuscode":413,
          "Statustext":"Api not found"
        },
        415:{
          "Statuscode":415,
          "Statustext":"The Api is either expire or the UUID is invalid"
        },
        416:{
          "Statuscode":416,
          "Statustext":"Invalid Ascess Token"
        },
        417:{
          "Statuscode":417,
          "Statustext":"Invalid Instance Id"
        },
        418:{
          "Statuscode":418,
          "Statustext":"UUID requires to ascess the api"
        },
        419:{
          "Statuscode":419,
          "Statustext":"This api contains an error"
        },
        421:{
          "Statuscode":421,
          "Statustext":"You have exceed the maximum attemt to login.So you are blocked "
        }
};
exports.Status=status_code;
