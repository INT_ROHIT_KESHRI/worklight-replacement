var cachingapi={
	 identifier:function(){
		 var returnedObject = {};
		 var loopback= require('loopback');
		 returnedObject["app"] = module.exports = loopback();
		 returnedObject["properties"] = require('./config/properties.js');
		 returnedObject["dataSource"] = require('./config/datasource.js');
		 return returnedObject;
		 },
	 getApi:function(url,cb){
		var identifiers=this.identifier();
		identifiers.app.model('apicaching',{dataSource: identifiers.dataSource.Datasource,properties: identifiers.properties.Cachingproperties});
		identifiers.app.model('apis',{dataSource: identifiers.dataSource.Datasource,properties: identifiers.properties.Apiproperties});
		var Apicashing = identifiers.app.models.apicaching;
		var Apis = identifiers.app.models.apis;
		Apis.find({where:{api:url} }, function(err, data) {
			if(data.length>0){
			Apicashing.find({where:{api_id:data[0].id,flag:1}},function(error,result){
					if(result.length > 0){
						cb(result);
					}else{
						//cb(false);
						cb(false);
					}			
			});
			}else{
				//cb(false);
				cb(false);
			}
			
		  });
	 }
};
//var LocationHelper = function() {};
//cachingapi.prototype.apiss=getApi(url,)
exports.cachingApi = cachingapi;
