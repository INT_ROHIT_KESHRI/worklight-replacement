var DataSource = require('loopback-datasource-juggler').DataSource;
var dataSource = new DataSource({
    connector: require('loopback-connector-mysql'),
    host: 'localhost',
    port: 3306,
    database: 'kwallet2_angular',
	username:'root',
	password:''
});
exports.Datasource=dataSource;
