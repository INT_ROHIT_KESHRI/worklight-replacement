var tokensecret = {
    lbinstance_id: {
        type: String,
        required: true
    },
    tokensecret: {
        type: String,
        required: false
    },
    created: {
        type: Date,
        required: true
    },
    updated: {
        type: Date,
        required: true
    },
    expiretime: {
        type: Date,
        required: false
    },
    user_unique_identity: {
        type: String,
        required: false
    },
    service: {
        type: String,
        required: false
    }
};
var ipaddress = {
    ipaddress: {
        type: String,
        required: true
    },
    flag: {
        type: String,
        required: false
    },
    created: {
        type: Date,
        required: true
    },
    modified: {
        type: Date,
        required: true
    }
};
var ratelimiter = {
    duration: {
        type: Number,
        required: true
    },
    hits: {
        type: Number,
        required: true
    },
    remaininghits: {
        type: Number,
        required: true
    },
    created: {
        type: Date,
        required: true
    },
    flag: {
        type: Number,
        required: true
    },
    modified: {
        type: Date,
        required: true
    }
};
var apis = {
    name: {
        type: String,
        required: true
    },
    method: {
        type: String,
        required: true
    },
    uri: {
        type: String,
        required: true
    },
    url: {
        type: String,
        required: true
    },
    model_id: {
        type: Number,
        required: true
    },
    client_id: {
        type: Number,
        required: true
    },
    token: {
        type: String,
        required: true
    },
    flag: {
        type: Number,
        required: false
    },
    is_logout: {
        type: Number,
        required: false
    },
    fields: {
        type: String,
        required: true
    },
    created: {
        type: Date,
        required: true
    },
    modified: {
        type: Date,
        required: true
    }
};
var cache = {
    api_id: {
        type: Number,
        required: true
    },
    duration: {
        type: String,
        required: false
    },
    flag: {
        type: Number,
        required: false
    },
    cachkey: {
        type: String,
        required: false
    },
    created: {
        type: Date,
        required: true
    },
    modified: {
        type: Date,
        required: true
    }
};
var responsetime = {
    api_id: {
        type: Number,
        required: true
    },
    time: {
        type: String,
        required: true
    },
    status: {
        type: String,
        required: true
    },
    flag: {
        type: Number,
        required: true
    },
    created: {
        type: Date,
        required: true
    },
    modified: {
        type: Date,
        required: true
    }
};
var model = {
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    UUID: {
        type: String,
        required: true
    },
    client_id: {
        type: Number,
        required: true
    },
    tracking_id: {
        type: String,
        required: false
    },
    flag: {
        type: Number,
        required: true
    },
    created: {
        type: Date,
        required: true
    },
    modified: {
        type: Date,
        required: true
    }
};
var apifields = {
    api_id: {
        type: Number,
        required: true
    },
    fields: {
        type: String,
        required: true
    },
    created: {
        type: Date,
        required: true
    },
    modified: {
        type: Date,
        required: true
    }
}
var auth = {
    api_id: {
        type: Number,
        required: true
    },
    model_id: {
        type: Number,
        required: true
    },
    function_name: {
        type: String,
        required: true
    },
		flag: {
				type: Number,
				required: true
		},
    restrict_login: {
      type: Number,
      required: true
    },
    max_attempts: {
      type: Number,
      required: true
    },
    block_time: {
      type: Number,
      required: true
    },
    attempt_interval: {
      type: Number,
      required: true
    },
    file_name: {
      type: String,
      required: true
    },
    created: {
        type: Date,
        required: true
    },
    modified: {
        type: Date,
        required: true
    }
}
var logincounter = {
    cookie: {
        type: String,
        required: true
    },
    counter: {
        type: Number,
        required: true
    },
    model_id:{
        type: Number,
        required: true
    },
    updated: {
        type: Date,
        required: true
    },
		created: {
				type: Date,
				required: true
		}
}
exports.Responsetime = responsetime;
exports.Cache = cache;
exports.Ratelimiter = ratelimiter;
exports.Apis = apis;
exports.Tokensecret = tokensecret;
exports.Ipaddress = ipaddress;
exports.Models = model;
exports.Apifields = apifields;
exports.Auth = auth;
exports.Logincounter = logincounter;
